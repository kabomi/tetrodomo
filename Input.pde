// Tetrodomo
// Creado por Imobach Martín 
// kabomi.es
class Input{
  PImage[] buttonImg;
  Button[] Buttons;
  Timer transparencyTimer;
  Point baseDrawPoint;
  
  PImage[] musicImages;
  PImage[] showControlImages;
  
  int numButtons = 6;
  char pressedButton;
  
  boolean visibleControls = false;
  
  Input(int musicState, int controlsState){
    this.buttonImg = new PImage[4];
    this.buttonImg[0] = loadImage(FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_RIGHT_ARROW);
    this.buttonImg[1] = loadImage(FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_LEFT_ARROW);
    this.buttonImg[2] = loadImage(FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_DOWN_ARROW);
    this.buttonImg[3] = loadImage(FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_REFRESH);
    this.Buttons = new Button[numButtons];
    
    musicImages = new PImage[3];
    musicImages[0] = loadImage(FILE_IMG_GAME_NO_MUSIC);
    musicImages[1] = loadImage(FILE_IMG_GAME_MUSIC_1);
    musicImages[2] = loadImage(FILE_IMG_GAME_MUSIC_2);
    showControlImages = new PImage[2];
    showControlImages[0] = loadImage(FILE_IMG_GAME_HIDE_CONTROLS);
    showControlImages[1] = loadImage(FILE_IMG_GAME_SHOW_CONTROLS);
    
    this.loadButtons();
    
    this.Buttons[_MUSIC_BUTTON_INDEX].setState(musicState);
    this.Buttons[_CONTROLS_BUTTON_INDEX].setState(controlsState);
    
    transparencyTimer = new Timer(_BUTTONS_TRANSPARENCY_LENGTH_MS);
    pressedButton = _NO_BUTTON_PRESSED;
  }
  void loadButtons(){
    this.Buttons[0] = new Button(new PImage[]{this.buttonImg[1]}, BUTTON_LEFT_RECT, _LEFT);
    this.Buttons[1] = new Button(new PImage[]{this.buttonImg[2]}, BUTTON_DOWN_RECT, _DOWN);
    this.Buttons[2] = new Button(new PImage[]{this.buttonImg[0]}, BUTTON_RIGHT_RECT, _RIGHT);
    this.Buttons[3] = new Button(new PImage[]{this.buttonImg[3]}, BUTTON_ROTATE_RECT, _ROTATE);
    this.Buttons[4] = new Button(musicImages                    , OPTIONS_RECT, _MUSIC);
    this.Buttons[5] = new Button(showControlImages              , OPTIONS_RECT.copyAndAdd(0, OPTIONS_RECT.height + OPTIONS_SEPARATION_Y), _CONTROLS_VIEW);
  }
  
  void Draw(){
    int startDrawIndex = (visibleControls?0:4);
    for(int i = startDrawIndex; i < numButtons; i++){
      this.Buttons[i].Draw();
    }
    if(visibleControls && !transparencyTimer.isFinished()){
      if(getPressedButton() != null){
        getPressedButton().DrawTransparency();
      }
    }
  }
  
  char getButtonFromPoint(Point newPoint){
    return getButtonFromPoint(newPoint.x, newPoint.y);
  }
  char getButtonFromPoint(int posx, int posy){
    //THIS METHOD IS USED IN ANDROID (TOUCH EVENTS)
    int startButtonIndex = (visibleControls?0:4);
    for(int i = startButtonIndex; i < numButtons; i++){
      if(this.Buttons[i].isPointWithinBoundaries(new Point(posx, posy))){
        return this.Buttons[i].type;
      }
    }
    return _NO_BUTTON_PRESSED;
  }
  
  Button getButton(char test){
    for(int i = 0; i < numButtons; i++){
      if(this.Buttons[i].type == test){
        return this.Buttons[i];
      }
    }
    return null;
  }
  Button getPressedButton(){
    return getButton(pressedButton);
  }
  
  void setPressedButton(char test){
    pressedButton = test;
    transparencyTimer.start();
  }
  
  void changeMusicState(int newState){
    this.Buttons[_MUSIC_BUTTON_INDEX].setState(newState);
  }
  void changeMusicState(){
    this.Buttons[_MUSIC_BUTTON_INDEX].changeState();
  }
  void changeControlsVisibility(int newState){
    this.Buttons[_CONTROLS_BUTTON_INDEX].setState(newState);
    if(this.Buttons[_CONTROLS_BUTTON_INDEX].state == _CONTROLS_HIDE)
      visibleControls = false;
    else
      visibleControls = true;
  }
  void changeControlsVisibility(){
    this.Buttons[_CONTROLS_BUTTON_INDEX].changeState();
    if(this.Buttons[_CONTROLS_BUTTON_INDEX].state == _CONTROLS_HIDE)
      visibleControls = false;
    else
      visibleControls = true;
  }
  int getMusicState(){
   return this.Buttons[_MUSIC_BUTTON_INDEX].state;
  }
  int getControlsState(){
   return this.Buttons[_CONTROLS_BUTTON_INDEX].state;
  }
}

