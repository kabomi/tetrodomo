class GameSettings{
  private int numRows;
  private int numColumns;
  private int cellWidth;
  private int cellHeight;
  private int borderUB;
  private int borderLR;
  
  private char mode;
  private boolean ghost;
  private int buffer;
  Piece[] pieces;
  PImage[] bufferImg;
  PImage[] squareImg;
  private int numLevels;
  private int[] levelLines;
  private float[] levelSpeed;
  public int lockTimeStop;
  public int lockTimeTop;
  private int[] linePoints;
  private char[] pieceTypes;
  public int numShapeType;
  public int shapeType;
  
  int musicChoice;
  int controlsChoice;
  
  GameSettings(char gameMode){
      this.numRows = ROWS_NUM;
      this.numColumns = COLUMNS_NUM;
      this.cellWidth = BOARD_CELL_DIM.width;
      this.cellHeight = BOARD_CELL_DIM.height;
      this.borderUB = BOARD_BORDER_UB;
      this.borderLR = BOARD_BORDER_LR;
      this.mode = gameMode;
      this.buffer = BUFFER_PIECES_NUM;
      this.pieces = new Piece[this.buffer];
      this.ghost = true;
      this.numLevels = 9;
      this.levelLines = new int[]{ 8, 8, 8, 9, 9, 10, 11, 12, 13 };
      this.levelSpeed = new float[]{ 0.6, 0.55, 0.5, 0.45, 0.4, 0.35, 0.3, 0.25, 0.2 };
      this.lockTimeStop = 1;
      this.lockTimeTop = 3;
      this.linePoints = new int[]{ 50, 100, 250, 650};
      this.pieceTypes = new char[] { 
        _SHAPE_RECT,
        _SHAPE_S,
        _SHAPE_Z,
        _SHAPE_BAR,
        _SHAPE_TRIANGLE,
        _SHAPE_P,
        _SHAPE_L,
        _SHAPE_TROMINO_BOOMERANG,
        _SHAPE_TROMINO_I,
        _SHAPE_DOMINO,
        _SHAPE_MONOMINO
      };
      this.numShapeType = _NUM_SHAPETYPES_FROM_ZERO;
      this.shapeType = _SHAPETYPE_RECT_IMG;
      
      this.musicChoice = _MUSIC_STATE_1;
      this.controlsChoice = _CONTROLS_HIDE;
      
      setMode();
  }
  void setMode(){
    switch(mode){
      case _GAME_MODE_NORMAL:
        this.pieceTypes = new char[] { 
          _SHAPE_RECT,
          _SHAPE_S,
          _SHAPE_Z,
          _SHAPE_BAR,
          _SHAPE_TRIANGLE,
          _SHAPE_P,
          _SHAPE_L
        };
        break;
      case _GAME_MODE_EXTREME:
        this.ghost = false;
        this.buffer = 5;
        this.pieces = new Piece[this.buffer];
        this.numLevels = 10;
        this.levelLines = new int[]{ 8, 8, 8, 9, 9, 10, 11, 12, 13, 13 };
        this.levelSpeed = new float[]{ 0.6, 0.55, 0.5, 0.45, 0.4, 0.35, 0.3, 0.25, 0.2, 0.15};
        this.lockTimeTop = 2;
        this.linePoints = new int[]{ 75, 150, 425, 1075};
        this.pieceTypes = new char[] { 
          _SHAPE_RECT,
          _SHAPE_S,
          _SHAPE_Z,
          _SHAPE_BAR,
          _SHAPE_TRIANGLE,
          _SHAPE_P,
          _SHAPE_L
        };
        break;
    }
  }
  
  void init(){
    this.bufferImg = new PImage[this.pieceTypes.length + 1];
    this.squareImg = new PImage[this.pieceTypes.length + 1];
    for(int i= 0;i< this.pieceTypes.length;i++){
      this.bufferImg[i] = loadImage(FILENAMEPREFIX_IMG_PIECES + this.pieceTypes[i] + FILETYPE_PNG);
      this.squareImg[i] = loadImage(FILENAMEPREFIX_IMG_SQUARE + this.pieceTypes[i] + FILETYPE_PNG);
    }
  }
  
  int getWidth(){
    return this.numColumns * (this.cellWidth + this.borderLR);
  }
  int getHeight(){
    return this.numRows * (this.cellHeight + this.borderUB);
  }
  int getSpeed(int level){
    return (int)(levelSpeed[level] * 1000);
  }
  int getLockStop(int level){
    return getSpeed(level)*this.lockTimeStop;
  }
  int getLockTop(int level){
    return getSpeed(level)*this.lockTimeTop;
  }
  
  void nextShape(){
    this.shapeType = (this.shapeType + 1) % this.numShapeType ;
  }
  void prevShape(){   
    this.shapeType = (this.shapeType - 1) % this.numShapeType;
    if(this.shapeType < 0 ){
      this.shapeType = this.numShapeType - 1;
    }
  }
}
