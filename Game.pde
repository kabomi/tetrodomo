// Tetrodomo
// Created by Imobach Martín 
// kabomi.es
int drawCount = 0;
class Game{
  int gridx = 0;
  int gridy = 0;

  int level = 0;
  int points = 0;
  int totalRows = 0;
  int numRows = 0;
  boolean gameOver = false;
  boolean isNewPiece = true;
  
  Piece p = null;
  GameSettings Settings = null;
  Board board = null;
  CollisionMatrix CM = null;
  Input input = null;
  PImage backgroundImage = null;
  PImage boardImage = null;
  PImage logoImage = null;
  
  Timer globalTimer;
  Timer playTimer;
  Timer lockTimer;
  //Timer lockTimerStop;
  //Timer lockTopTimer;
  
  String eventsBuffer = "";
  
  char gameMode = _GAME_MODE_NORMAL;
  
  PFont gameScoreFont = createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_GAME_SCORE);
  PFont gameTitlesFont = createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_GAME_TITLES);
  
  Game(char mode){
    this.gameMode = mode;
    Init();
  }
  void Init(){
    drawCount = 0;
    eventsBuffer = "";
    level = 0;
    points = 0;
    totalRows = 0;
    numRows = 0;
    gameOver = false;
    Settings = new GameSettings(gameMode);
    loadSettings();
    setGrid();
    board = new Board(Settings);
    CM = CollisionMatrix.CollisionMatrixWithBorder();
    InitPieces();
    NewPiece();
    input = new Input(Settings.musicChoice, Settings.controlsChoice);
    changeMusic(Settings.musicChoice);
    input.changeControlsVisibility(Settings.controlsChoice);
    globalTimer = new Timer();
    setSpeed();
    //backgroundImage = loadImage(FILE_IMG_GAME_BACKGROUND);
    boardImage = loadImage(FILE_IMG_BOARD);
    logoImage = loadImage(FILE_IMG_LOGO_3);
  }
  void setSpeed(){
    playTimer = new Timer(Settings.getSpeed(level));
    playTimer.start();
  }
  void loadSettings(){
    try{
      String fileString = Utils.ReadAll(dataPath(FILE_JSON_SETTINGS));
      JSONorg.JSONObject json = new JSONorg.JSONObject(fileString);
      println("json file readed!");
      JSONorg.JSONObject jsonOptions = json.getJSONObject("Options");
      /*Settings = (GameSettings)Util.jsonToObj(obj, Settings);
      Settings.init();
      assert Settings.numColumns > 0 && Settings.numRows > 0;
      COLUMNS_NUM = Settings.numColumns;
      ROWS_NUM = Settings.numRows;*/
      Settings = new GameSettings(gameMode);
      Settings.init();
      Settings.musicChoice = jsonOptions.getInt("music");
      Settings.controlsChoice = jsonOptions.getInt("controls");
      println("GameSettings");
    }catch(Exception ex){
      println("Error al cargar json file:" + ex.getMessage());
      Settings = new GameSettings(gameMode);
      Settings.init();
    }
  }
  void saveSettings(){
    JSONorg.JSONObject json = new JSONorg.JSONObject();
    try{
      JSONorg.JSONObject jsonOptions = new JSONorg.JSONObject();
      //music
      jsonOptions.put("music", Settings.musicChoice);
      //controls
      jsonOptions.put("controls", Settings.controlsChoice);
      json.put("Options", jsonOptions);
      println("Settings:" + FILE_JSON_SETTINGS);
      Util.SaveAll(dataPath(FILE_JSON_SETTINGS), json.toString());
    }catch(Exception ex){
      println(ex.getMessage());
    }
  }
  boolean shouldMusicBePlaying(){
    return (Settings.musicChoice != _MUSIC_STATE_NO);
  }
  void setGrid(){
    this.gridx = BOARD_GRID_SEPARATION.x;
    this.gridy = BOARD_GRID_SEPARATION.y;
    println("gridx: " + this.gridx + ", gridy:" + this.gridy);
  }
  void NewPiece(){
    p = Settings.pieces[0];
    p.setSpeed(Settings.getLockStop(level), Settings.getLockTop(level));
    //lockTimerTop = new Timer(Settings.getLockTop());
    System.arraycopy(Settings.pieces,1,Settings.pieces,0,Settings.buffer - 1);
    Settings.pieces[Settings.buffer -1] = null;
    InitPieces();
    isNewPiece = true;
  }
  void InitPieces(){
    for(int i=0;i<Settings.buffer;i++){
      Logger.Log("BufferPiece:" + ((Settings.pieces[i] == null)?"NULL":Settings.pieces[i].type));
      if(Settings.pieces[i] == null){
        int index = int(random(Settings.pieceTypes.length));
        Piece newP = new Piece(Settings.pieceTypes[index], Settings);
        Settings.pieces[i] = newP;
      }
    }
  }
  
  boolean isCurrentPiecePlaced(){
    return p.isPlaced;
  }
  
  boolean isPaused(){
    return playTimer.isPaused;
  }
  
  void Pause(){
    globalTimer.Pause();
    playTimer.Pause();
    p.Pause();
    Logger.Log("Pause");
    Logger.Log("Elapsed Time:" + globalTimer.getElapsedTime());
  }
  
  void Resume(){
    globalTimer.Continue();
    playTimer.Continue();
    p.Continue();
    Logger.Log("Continue");
    Logger.Log("Elapsed Time:" + globalTimer.getElapsedTime());
  }
  
  void PlayLock(){
    lockTimer.start();
    //lockTopTimer.start();
  }
  
  void Save(){
    globalTimer.Pause();
    playTimer.Pause();
    p.Pause();
    JSONorg.JSONObject json = new JSONorg.JSONObject();
    try{
      //GameSettings
      json.put("Settings",Util.objToJSON(Settings));
      JSONorg.JSONArray pieces = new JSONorg.JSONArray();
      for(int i=0;i< Settings.pieces.length;i++){
        pieces.put(Util.objToJSON(Settings.pieces[i]));
      }
      json.getJSONObject("Settings").put("pieces", pieces);
      Logger.Log("jsonSettings:" + json.get("Settings").toString());
      //Board matrix
      json.put("Board", board.Matrix);
      Logger.Log("jsonBoard:" + json.get("Board").toString());
      //Collision matrix
      json.put("Collision", CM.Matrix);
      Logger.Log("jsonCollision:" + json.get("Collision").toString());
      //Piece p
      json.put("Piece",Util.objToJSON(p));
      Logger.Log("jsonPiece:" + json.get("Piece").toString());
      JSONorg.JSONObject jsonGlobal = new JSONorg.JSONObject();
      //level
      jsonGlobal.put("level",level);
      //points
      jsonGlobal.put("points",points);
      //totalRows
      jsonGlobal.put("totalRows",totalRows);
      //numRows
      jsonGlobal.put("numRows",numRows);
      json.put("Global", jsonGlobal);
      Logger.Log("Save:" + FILE_JSON_SAVEGAME + gameMode + FILETYPE_JSON);
      Util.SaveAll(dataPath(FILE_JSON_SAVEGAME + gameMode + FILETYPE_JSON), json.toString());
    }catch(Exception ex){
      Logger.Log(ex.getMessage());
    }
    globalTimer.Continue();
    playTimer.Continue();
    p.Continue();
  }
  
  void Load(){
    globalTimer.Pause();
    playTimer.Pause();
    p.Pause();
    JSONorg.JSONObject json = null;
    //Backup
    GameSettings oldSettings = Settings;
    char[][] oldBoardMatrix = board.Matrix;
    char[][] oldCMMatrix = CM.Matrix;
    Piece oldPiece = p;
    int oldLevel = level;
    int oldPoints = points;
    int oldTotalRows = totalRows;
    int oldNumRows = numRows;
    
    try{
      println("Load:" + FILE_JSON_SAVEGAME + gameMode + FILETYPE_JSON);
      json = new JSONorg.JSONObject(Utils.ReadAll(dataPath(FILE_JSON_SAVEGAME + gameMode + FILETYPE_JSON)));
      //GameSettings
      Settings = (GameSettings)Util.jsonToObj(new JSONorg.JSONObject(json.get("Settings").toString()), Settings);
      //Buffer pieces
      JSONorg.JSONArray pieces = json.getJSONObject("Settings").getJSONArray("pieces");
      Logger.Log("pieces:" + pieces.toString());
      //Load Settings
      loadSettings();
      Settings.pieces = new Piece[pieces.length()];
      for(int i=0;i<pieces.length();i++){
        JSONorg.JSONObject jsonPiece = pieces.getJSONObject(i);
        Piece piece = new Piece(jsonPiece.getString("type").charAt(0), Settings);
        Settings.pieces[i] = (Piece)Util.jsonToObj(jsonPiece, piece);
      }
      //Settings.init();
      Logger.Log("Settings loaded");

      board.Matrix = Util.jsonArrayToCharMatrix(json.getJSONArray("Board"));
      Logger.Log("Board loaded");

      CM.Matrix = Util.jsonArrayToCharMatrix(json.getJSONArray("Collision"));
      Logger.Log("Board loaded");
      
      JSONorg.JSONObject jsonPiece = json.getJSONObject("Piece");
      Piece newPiece = new Piece(jsonPiece.getString("type").charAt(0), Settings);
      p = (Piece)Util.jsonToObj(jsonPiece, newPiece);
      p.setSpeed(Settings.getLockStop(level), Settings.getLockTop(level));
      Logger.Log("Piece loaded");
      
      JSONorg.JSONObject jsonGlobal = json.getJSONObject("Global");
      level = jsonGlobal.getInt("level");
      points = jsonGlobal.getInt("points");
      totalRows = jsonGlobal.getInt("totalRows");
      numRows = jsonGlobal.getInt("numRows");
      Logger.Log("Global loaded");
      setSpeed();
      globalTimer = new Timer();
      globalTimer.start();
    }catch(Exception ex){
      Logger.Log(ex.getMessage());
      println("Could not load file. Restoring Backup");
      //Restore Backup
      Settings = oldSettings;
      board.Matrix = oldBoardMatrix;
      CM.Matrix = oldCMMatrix;
      p = oldPiece;
      level = oldLevel;
      points = oldPoints;
      totalRows = oldTotalRows;
      numRows = oldNumRows;
    }
    globalTimer.Continue();
    playTimer.Continue();
    p.Continue();
  }
  
  void Draw(){ 
      //Logger.active = true;
      background(BACKGROUND_R, BACKGROUND_G, BACKGROUND_B);
      drawTexts();
      pushMatrix();
      scale(Scale.scaleWidth, Scale.scaleHeight);
      
      if(drawCount < Integer.MAX_VALUE){
        drawCount++;
      }else{
        drawCount = 0;
      }
      
      if(!p.isPlaced)
        flushEvents();
      else
        clearEvents();
      
      drawImage(boardImage, BOARD_IMG_RECT);
      drawImage(logoImage, BOARD_IMG_LOGO_RECT);
      fill(_WHITE);
      stroke(0);
      
      //Buffer
      drawBuffer();
      pushMatrix();
      translate(gridx,gridy);

      boolean[][] pieceMatrix = p.getState();
      PointMatrix pMatrix = PointMatrix.PointMatrix(pieceMatrix, Point.Point(p.x,p.y));
      if(p.isPlaced && !gameOver){
        Logger.Log("Pieza Guardada. Nueva Pieza");
        board.Print();
        Logger.Log("--------------");
        CM.Print();
        Logger.Log("--------------");
        board.Save(p);
        Logger.Log("--------------AFTER-------------");
        board.Print();
        CM.save(pMatrix);
        Logger.Log("--------------");
        CM.Print();
        
        //Eliminar filas completas
        String fullRows = board.getFullRows();
        if(fullRows.length() > 0){
          Logger.Log("Se eliminan " + split(fullRows, '|').length  + " filas(" + fullRows + ").");
          board.manipulateGivenRows(fullRows, _DELETE);
          Logger.Log("--------------DELETE-------------");
          board.Print();
          CM.manipulateGivenRows(fullRows, _DELETE);
          Logger.Log("--------------");
          CM.Print();
          updateScore(split(fullRows, '|').length);
        }else{
          Effects.playMediaFile(FILE_EFFECT_PIECE_PLACED);
        }
        playTimer.start();
        NewPiece();
      }else{
        drawBoard(pMatrix);
        if(gameOver){
          popMatrix();
          popMatrix();
          fill(_BLACK);
          drawText(mainFont, GAME_TEXT_GAME_OVER, GAME_TEXT_GAMEOVER_RECT);
          println("Before noLoop");
          noLoop();
          println("After noLoop");
        }
      }
      if (playTimer.isFinished() && !p.isPlaced) {
        Logger.Log("Velocidad: " + Settings.getSpeed(level));
        pMatrix.moveTo(new Point(p.x , p.y + 1));
        if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
          p.y+= 1;
          p.UnLock();
        }else{
          if(!p.isLocked)
            p.Lock(true);      
          else
            p.Lock(false);
        }
        playTimer.start();
      }
      if(!gameOver){
        popMatrix();
        popMatrix();
        if(isNewPiece){
          CheckGameOver();
        }
      }
      //Botones
      translate(0, Scale.translateY);
      drawInput();
      //Logger.active = false;
  }
  void drawTexts(){
    fill(_WHITE);
    drawText(gameScoreFont, str(points), GAME_TEXT_SCORE_NUMBERS_RECT);
    drawText(gameTitlesFont, GAME_TEXT_SCORE, GAME_TEXT_SCORE_RECT);
    drawText(str(level + 1), GAME_TEXT_LEVEL_NUMBERS_RECT);
    drawText(GAME_TEXT_LEVEL, GAME_TEXT_LEVEL_RECT);
    drawText(GAME_TEXT_NEXT, GAME_TEXT_NEXT_RECT);
    textFont(mainFont);
  }
  void drawInput(){
    input.Draw();
  }
  void drawBuffer(){
    Rect bufferRect = BUFFER_PIECE_RECT.copy();
    for(int i=0; i< Settings.buffer;i++){
      int index = 0;
      for(int j=0;j < Settings.pieceTypes.length;j++){
        if(Settings.pieceTypes[j] == Settings.pieces[i].type){
          index = j;
          break;
        }
      }
      drawImage(Settings.bufferImg[index], bufferRect);
      bufferRect.moveY(BUFFER_PIECE_RECT.height);
    }
  }
  void drawBoard(PointMatrix pMatrix){
    try{
      if(Settings.ghost){
        drawGhost(pMatrix);
      }
      else{
        board.Draw(p);
      }
    }catch(Exception ex){
      println("Error:" + ex.getMessage());
      ex.printStackTrace(System.out);
      println("-----------------------");
      board.Print();
      println("-----------------------");
      CM.Print();
      println("-----------------------");
      p.Print();
      println("-----------------------");
      Pause();
    }
  }
  void drawGhost(PointMatrix pMatrix){
    Point ghostRef = new Point(p.x, p.y);
    ghostRef.moveY(1);
    pMatrix.moveTo(ghostRef);
    while(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
      ghostRef.moveY(1);
      pMatrix.moveTo(ghostRef);
    }
    ghostRef.moveY(-1);
    board.Draw(p, ghostRef.y);
  }
  void updateScore(int Rows){
    numRows = numRows + Rows;
    if(updateLevel()){
      Effects.playMediaFile(FILE_EFFECT_LEVEL_UP);
    }else{
      Effects.playMediaFile(FILE_EFFECT_LINES);
    }
    totalRows += Rows;
    for(int i = 0; i < Rows; i++){
      points += Settings.linePoints[i];
    }
  }
  boolean updateLevel(){
    int levelLines = Settings.levelLines[level];
    if(numRows >= levelLines && (level + 1) < Settings.numLevels){
      level ++;
      playTimer.Pause();
      println("Nivel completado en: " + str(playTimer.getElapsedTime()/1000) + "seg");
      setSpeed();
      Logger.Log("Se alcanza el nivel: " + level);
      numRows = numRows % levelLines;
      return true;
    }
    return false;
  }
  void CheckGameOver(){
    PointMatrix pMatrix = PointMatrix.PointMatrix(p.getState(), Point.Point(p.x,p.y));
    if(!CM.isBorderedMatrixWithinBoundaries(pMatrix)){
      GameOver();
    }
    isNewPiece = false;
  }
  void GameOver(){
    Sound.pause();
    Effects.playMediaFile(FILE_EFFECT_GAME_OVER);
    gameOver = true;
    saveStats();
    globalTimer.Pause();
    println("Juego terminado en: " + str(globalTimer.getElapsedTime()/1000) + "seg");
    Logger.Log("\nTermino el Juego\n");
    loadStats();
  }
  void saveStats(){
    String date = year() + "/" + month() + "/" + day() + " " + hour() + ":" + minute();
    JSONorg.JSONObject json = null;
    JSONorg.JSONArray loadedStats = null;
    try{
      json = new JSONorg.JSONObject(Utils.ReadAll(dataPath(FILE_JSON_STATS + gameMode + FILETYPE_JSON)));
      loadedStats = json.getJSONArray("STATS");
      Logger.Log("STATS:" + loadedStats.toString());
    }catch(Exception ex){
      Logger.Log("Possible problem loading stats file: " + date + ": "  + ex.getMessage());
      try{
        Util.SaveAll(dataPath(FILE_JSON_STATS + gameMode + FILETYPE_JSON), "{STATS:" + "[]}");
        json = new JSONorg.JSONObject(Utils.ReadAll(dataPath(FILE_JSON_STATS + gameMode + FILETYPE_JSON)));
        loadedStats = json.getJSONArray("STATS");
        Logger.Log("STATS:" + loadedStats.toString());
      }catch(Exception ex2){
        Logger.Log("Error creating stats file:" + ex2.getMessage());
      }
    }
    //DO stuff
    try{
      boolean currentStatsSaved = false;
      if(loadedStats.length() < STATS_SAVE_LIMIT){
        Stat stat = new Stat(points, level, totalRows, date);
        loadedStats.put(Util.objToJSON(stat));
        currentStatsSaved = true;
      }else{
        int lowerPoints = Integer.MAX_VALUE;
        int indexWithLowerPoints = -1;
        for(int i=0;i< STATS_SAVE_LIMIT;i++){
          Stat stat = new Stat();
          stat = (Stat)Util.jsonToObj(loadedStats.getJSONObject(i), stat);
          if(stat.points < lowerPoints){
            lowerPoints = stat.points;
            indexWithLowerPoints = i;
          }
          if(stat.points < points){
            Logger.Log("Save stats: " + Util.objToJSON(stat));
            stat.points = points;
            stat.level = level;
            stat.totalRows = totalRows;
            stat.date = date;
            loadedStats.put(Util.objToJSON(stat));
            currentStatsSaved = true;
            break;
          }
        }
        if(currentStatsSaved && indexWithLowerPoints >= 0){
          Logger.Log("Remove stats at " + indexWithLowerPoints);
          loadedStats.remove(indexWithLowerPoints);
        }
      }
      if(currentStatsSaved){
        json.put("STATS",loadedStats);
        Logger.Log("NEW STATS:" + json.toString());
        Util.SaveAll(dataPath(FILE_JSON_STATS + gameMode + FILETYPE_JSON), json.toString());
      }
    }catch(Exception ex){
      Logger.Log("Possible problem saving stats file " + date + ": " + ex.getMessage());
    }
  }
  
  void mouseClicked() {
    NewPiece();
  }
  
  void clearEvents(){
    eventsBuffer = "";
  }
  void flushEvents(){
    Logger.Log("flushEvents:" + eventsBuffer);
    try{
      while(eventsBuffer.length() > 0){
        char event = eventsBuffer.charAt(0);
        eventsBuffer = eventsBuffer.substring(1);
        raiseEvent(event);
      }
    }catch(Exception ex){
      println("Input Buffer Exception: " + ex.getMessage() + "||| eventsBuffer: " + eventsBuffer);
      ex.printStackTrace(System.out);
      noLoop();
    }
  }
  void inputEvent(char test){
    if(p.isPlaced){
      eventsBuffer = "";
      return;
    }
    if(eventsBuffer.length() < MAX_EVENTS_BUFFER){
      eventsBuffer = eventsBuffer + str(test);
    }
  }
  void raiseEvent(char test){
    int newState = p.state;
    input.setPressedButton(test);
    boolean[][] pieceMatrix;
    if(test == _ROTATE){
      newState = ((newState + 1) % (p.States.size()));
      pieceMatrix = (boolean[][])p.States.get(newState);
    }else{
      pieceMatrix = (boolean[][])p.getState();
    }
    PointMatrix pMatrix = PointMatrix.PointMatrixOnSet(pieceMatrix);
    
    switch(test){
      case _LEFT:
        pMatrix.moveTo(new Point(p.x - 1 , p.y));
        if(CM.isBorderedMatrixWithinBoundaries(pMatrix))
          p.x -= 1;
        break;
      case _RIGHT:
        pMatrix.moveTo(new Point(p.x + 1 , p.y));
        if(CM.isBorderedMatrixWithinBoundaries(pMatrix))
          p.x += 1;
        break;
      case _DOWN:
        pMatrix.moveTo(new Point(p.x , p.y + 1));
        if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
          p.y += 1;
          playTimer.start();
        }
        break;
      case _ROTATE:
        pMatrix.moveTo(new Point(p.x ,p.y));
        if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
          p.state = newState;
          return;
        }else{
          if(gameMode == _GAME_MODE_EASY_SPIN){
            //comprobación hacia arriba para ver si encaja
            pMatrix.moveTo(new Point(p.x ,p.y - 1));
            if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
              p.state = newState;
              p.y -= 1;
              return;
            }else{
              pMatrix.moveTo(new Point(p.x ,p.y - 2));
              if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
                p.state = newState;
                p.y -= 2;
                return;
              }else{
                pMatrix.moveTo(new Point(p.x ,p.y - 3));
                if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
                  p.state = newState;
                  p.y -= 3;
                  return;
                }
              }
            }
          }
          //desplazamiento a la izq para ver si encaja
          pMatrix.moveTo(new Point(p.x - 1 ,p.y));
          if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
            p.state = newState;
            p.x--;
            return;
          }
          //desplazamiento a la derecha para ver si encaja
          pMatrix.moveTo(new Point(p.x + 1 ,p.y));
          if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
            p.state = newState;
            p.x++;
            return;
          }
          //doble desplazamiento izq
          pMatrix.moveTo(new Point(p.x - 2 ,p.y));
          if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
            p.state = newState;
            p.x = p.x - 2;
            return;
          }
          //doble desplazamiento derecha
          pMatrix.moveTo(new Point(p.x + 2 ,p.y));
          if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
            p.state = newState;
            p.x = p.x + 2;
            return;
          }
          
          /*
          //cambio extra de estado 1
          newState = ((newState + 1) % (p.States.size()));
          if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
            p.state = newState;
            return;
          }
          //cambio extra de estado 2
          newState = ((newState + 1) % (p.States.size())); 
          if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
            p.state = newState;
            return;
          }
          //cambio extra de estado 3
          newState = ((newState + 1) % (p.States.size()));
          if(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
            p.state = newState;
            return;
          }*/
        }
        break;
      case _LEVEL_UP:
        if(level + 1 < Settings.numLevels){
          level ++;
          setSpeed();
        }
        Logger.Log("Se cambia al nivel: " + level);
        break;
      case _LEVEL_DOWN:  
        if(level > 0){
          level --;
          setSpeed();
        }
        Logger.Log("Se cambia al nivel: " + level);
        break;
      case _MUSIC:
        println("Se cambia la musica");
        input.changeMusicState();
        Settings.musicChoice = input.getMusicState();
        changeMusic(Settings.musicChoice);
        saveSettings();
        break;
      case _CONTROLS_VIEW:
        println("Se cambia la visibilidad de los controles");
        input.changeControlsVisibility();
        Settings.controlsChoice = input.getControlsState();
        saveSettings();
        break;
      case _PIECE_DOWN:
        if(p.isPlaced)
          return;
        pMatrix.moveTo(new Point(p.x , p.y + 1));
        while(CM.isBorderedMatrixWithinBoundaries(pMatrix)){
          p.y += 1 ;
          pMatrix.moveTo(new Point(p.x , p.y + 1));
        }
        if(gameMode == _GAME_MODE_EXTREME){
          clearEvents();
          p.isPlaced = true;
        }
        break;
      case _PIECE_RESET:
        pMatrix.moveTo( new Point(p.x , 0));
        if(CM.isBorderedMatrixWithinBoundaries(pMatrix))
          p.y = 0;
        else
          GameOver();
        break;
    }
  }
  
  void printAllFonts(String filter){
    String[] fontList = PFont.list();
    int nextFont = 1;
    for(int i=0;i<fontList.length;i++){
      if(fontList[i].indexOf(filter) >= 0)
      {
        PFont myFont = createFont(fontList[i], 14);
        textFont(myFont);
        text(fontList[i], 20, 20*nextFont);
        println("'" + fontList[i] + "'");
        nextFont++;
      }
    }
  }
}

