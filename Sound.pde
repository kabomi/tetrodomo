//import ddf.minim.*;

class MediaPlayer{
  final boolean isAndroid = Util.isAndroid();
  //Minim minim;
  //AudioPlayer player;
  //AudioInput input;
  //APWidgets
  APMediaPlayer player;
  int playPosition = 0;
  boolean looping = true;
  
  public void MediaPlayer(){
    ;
  }
  public void setup(PApplet pApplet){
    setup(pApplet, null);
  }
  public void setup(PApplet pApplet, String mediaFile){
    //minim = new Minim(this);
    //player = minim.loadFile("TDeeJay_-_DisTOrATion.mp3");
    //input = minim.getLineIn();
    if(!isAndroid)
      return;
    release();
    player = new APMediaPlayer(pApplet); //create new APMediaPlayer
    player.setVolume(0.5, 0.5); //Set left and right volumes. Range is from 0.0 to 1.0
    if(mediaFile != null)
      setMediaFile(mediaFile); //set the file (files are in data folder)
    //player.start(); //start play back
    //player.setLooping(true);//restart playback end reached
  }
  void setMediaFile(String file){
    if(!isAndroid)
      return;
    player.setMediaFile(file);
  }
  void playMediaFile(String file){
    if(!isAndroid)
      return;
    try{
      player.setVolume(0.5f, 0.5f);
      setMediaFile(file);
      playFromStart();
      println("Sound Effect played: " + file);
    }catch(Exception ex){
      println("Error trying to play " + file + ". Error:" + ex.getMessage()); 
    }
  }
  void setLooping(boolean looping){
    if(!isAndroid)
      return;
    this.looping = looping;
  }
  void play(){
    if(!isAndroid)
      return;
    startPlaying();
  }
  private void startPlaying(){
    if(!isAndroid)
      return;
    println("startPlaying");
    player.seekTo(playPosition);
    player.start();
    player.setLooping(looping);
  }
  void playFromStart(){
    if(!isAndroid)
      return;
    println("playFromStart");
    playPosition = 0;
    startPlaying();
  }
  void pause(){
    if(!isAndroid)
      return;
    playPosition = player.getCurrentPosition();
    player.pause();
  }
  void setPosition(int position){
    if(!isAndroid)
      return;
    playPosition = position;
  }
  void release(){
    if(!isAndroid)
      return;
    // the AudioPlayer you got from Minim.loadFile()
    //player.close();
    // the AudioInput you got from Minim.getLineIn()
    //input.close();
    //minim.stop();
    if(player!=null) { //must be checked because or else crash when return from landscape mode
      player.release(); //release the player
      println("Player released");
    }
  }
}

