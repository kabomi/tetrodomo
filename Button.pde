class Button{
  char type;
  Rect rect;
  PImage[] stateImg;
  int numStates;
  int state;
  boolean fixedButton;
  boolean transparency;
  
  
  Button(PImage[] images, Rect rect, char type){
    numStates = images.length;
    stateImg = images;
    state = 0;
    this.rect = rect;
    this.type = type;
    fixedButton = false;
    transparency = true;
  }
  boolean isPointWithinBoundaries(Point point){
    if(point.x >= rect.point.x && point.x <= (rect.point.x + rect.width) && point.y >= rect.point.y && point.y <= (rect.point.y + rect.height))
      return true;
    return false;
  }
  void DrawTransparency(){
    fill(BUTTON_BACKGROUND_COLOR, _ALMOST_TRANSPARENT);
    stroke(BUTTON_BACKGROUND_COLOR, _TRANSPARENT);
    rect(rect.point.x, rect.point.y, rect.width, rect.height);
  }
  void Draw(){  
    tint(_WHITE, _BIT_TRANSPARENT);
    switch(this.type){
      case _LEFT:     
        drawImage(stateImg[state], BUTTON_IMAGE_LEFT_RECT);
        break;
      case _RIGHT:
        drawImage(stateImg[state], BUTTON_IMAGE_RIGHT_RECT);
        break;
      case _DOWN:
        drawImage(stateImg[state], BUTTON_IMAGE_DOWN_RECT);
        break;
      case _ROTATE:
        drawImage(stateImg[state], rect);
        break;
      default:
        noTint();
        drawImage(stateImg[state], rect);
        break;
    }
  }
  void changeState(){
    state = (state + 1) % numStates;
  }
  void setState(int newState){
    if(state > -1 && state < numStates)
      state = newState;
  }
}
class FixedButton extends Button{
  FixedButton(PImage[] images, Rect rect, char type){
    super(images, rect, type);
    fixedButton = true;
    transparency = false;
  }
}
