static class Dim{
  int width;
  int height;
  
  static Dim Dim(int px, int py){
    return new Dim(px,py);
  }
  static Dim DimOnSet(){
    return new Dim(0,0);
  }
  
  Dim(int width, int height){
    this.width = width;
    this.height = height;
  }
  
  void moveX(int units){
    width += units;
  }
  void moveY(int units){
    height += units;
  }
  void moveBoth(int units){
    moveX(units);
    moveY(units);
  }
  boolean equals(Dim dim){
    return (this.width == dim.width) && (this.height == dim.height);
  }

}
