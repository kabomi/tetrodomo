// Tetrodomo
// Created by Imobach Martín 
// kabomi.es
class Piece{
  public char type;
  public int state;
  private int sizex;
  private int sizey;
  private ArrayList States;
  public boolean ghost;
  public int shapeType;
  public int x;
  public int y;
  
  private Timer lockTimerStop;
  private Timer lockTimerTop;
  private int lockStop;
  private int lockTop;
  public boolean isLocked;
  public boolean isPlaced;
  
  Piece(char e, GameSettings Settings){
    this.sizex = 4;
    this.sizey = 4;
    this.x = floor((Settings.numColumns - sizex) / 2);
    if(Settings.mode == _GAME_MODE_EASY_SPIN){
      this.y = 0;
    }else{
      this.y = -3;
    }
    this.state = 0;
    this.type = e;
    this.shapeType = Settings.shapeType;
    this.ghost = Settings.ghost;
    InitStates(e,Settings.mode);
    //lockTimerStop = new Timer(Settings.getLockStop());
    //lockTimerTop = new Timer(Settings.getLockTop());
    this.isLocked = false;
    this.isPlaced = false;
  }
  void setSpeed(int lockStop, int lockTop){
    this.lockStop = lockStop;
    this.lockTop = lockTop;
    lockTimerStop = new Timer(lockStop);
    lockTimerTop = new Timer(lockTop);
  }
  void InitStates(char piece, char mode){
    this.States = new ArrayList();
    String raw = "", rawES = "";
    switch(piece){
      case _SHAPE_RECT:
          //"0000.0000.0110.0110"
          raw = "0000000001100110";
          //"0110.0110.0000.0000"
          rawES="0110011000000000";
        break;
      case _SHAPE_S:
          //"0000.0000.0011.0110/0000.0100.0110.0010"
          raw = "00000000011011000000010001100010";
          //"0110.1100.0000.0000/0100.0110.0010.0000"
          rawES="01101100000000000100011000100000";
        break;
      case _SHAPE_Z:
          //"0000.0000.0110.0011/0000.0010.0110.0100"
          raw = "00000000011000110000001001100100";
          //"0010.0110.0100.0000/0110.0011.0000.0000"
          rawES="00100110010000000110001100000000";
        break;
      case _SHAPE_BAR:
          //"0000.0000.0000.1111/0010.0010.0010.0010/0000.0000.0000.1111/0100.0100.0100.0100"
          raw = "0000000000001111001000100010001000000000000011110100010001000100";
          //"0000.1111.0000.0000/0100.0100.0100.0100/0000.0000.1111.0000/0010.0010.0010.0010"
          rawES="0000111100000000010001000100010000000000111100000010001000100010";
        break;
      case _SHAPE_TRIANGLE:
          //"0000.0000.0111.0010/0000.0010.0110.0010/0000.0000.0010.0111/0000.0100.0110.0100"
          raw = "0000000001110010000000100110001000000000001001110000010001100100";
          //"0100.1110.0000.0000/0100.0110.0100.0000/0000.1110.0100.0000/0100.1100.0100.0000"
          rawES="0100111000000000010001100100000000001110010000000100110001000000";
        break;
      case _SHAPE_P:
          //"0000.0000.0111.0001/0000.0010.0010.0110/0000.0000.0100.0111/0000.0110.0100.0100"
          raw = "0000000001110001000000100010011000000000010001110000011001000100";
          //"0110.0100.0100.0000/0000.1110.0010.0000/0100.0100.1100.0000/1000.1110.0000.0000"
          rawES="0110010001000000000011100010000001000100110000001000111000000000";
        break;
      case _SHAPE_L:
          //"0000.0000.0111.0100/0000.0110.0010.0010/0000.0000.0001.0111/0000.0100.0100.0110"
          raw = "0000000001110100000001100010001000000000000101110000010001000110";
          //"1100.0100.0100.0000/0010.1110.0000.0000/0100.0100.0110.0000/0000.1110.1000.0000"
          rawES="1100010001000000001011100000000001000100011000000000111010000000";
        break;
      case _SHAPE_TROMINO_BOOMERANG:
          //"0000.0000.0010.0110/0000.0000.0100.0110/0000.0000.0110.0100/0000.0000.0110.0010"
          raw = "0000000000100110000000000100011000000000011001000000000001100010";
          //"0010.0110.0000.0000/0100.0110.0000.0000/0110.0100.000.000/0110.0010.0000.0000"
          rawES="0010011000000000010001100000000001100100000000000110001000000000";
        break;
      case _SHAPE_TROMINO_I:
          //"0000.0000.0000.1110/0000.0100.0100.0100/0000.0000.0000.0111/0000.0010.0010.0010"
          raw = "0000000000001110000001000100010000000000000001110000001000100010";
          //"0100.0100.0100.0000/0111.0000.0000.0000/0010.0010.0010.0000/1110.0000.0000.0000"
          rawES="010001000100000001110000000000000010.0010.0010.00001110.0000.0000.0000";
        break;
      case _SHAPE_DOMINO:
          //"0000.0000.0100.0100/0000.0000.0000.0110/0000.0000.0010.0010/0000.0000.0000.0110"
          raw = "0000000001000100000000000000011000000000001000100000000000000110";
          //"0100.0100.0000.0000/0110.0000.0000.0000/0010.0010.0000.0000/0110.0000.0000.0000"
          rawES="0100010000000000011000000000000000100010000000000110000000000000";
        break;
      case _SHAPE_MONOMINO:
          //"0000.0000.0000.0100"
          raw = "0000000000000100";
          //"0100.0000.0000.0000"
          rawES="0100000000000000";
        break;
    }
    if(mode == _GAME_MODE_EASY_SPIN)
      addStatesFrom(rawES);
    else
      addStatesFrom(raw);
  }
  void addStatesFrom(String raw){
    if(raw.length() > 0){
      int numStates = raw.length() / (this.sizex * this.sizey);
      int rawIndex = 0;
      
      for(int n = 0; n < numStates; n++){
        boolean[][] matrix = new boolean[this.sizex][this.sizey];
        for(int row= 0; row < this.sizey;row++){
          for(int col= 0; col < this.sizex;col++){
            if(raw.charAt(rawIndex) == '1'){
              matrix[col][row] = true;
            }else{
              matrix[col][row] = false;
            }
            rawIndex++;
          }
        }
        this.States.add(matrix);
      }
    }
  }
  
  boolean[][] getState(){
    return (boolean[][])States.get(state);
  }
  
  void ChangeShape(int shapeType){
    this.shapeType = shapeType;
  }
  
  boolean Lock(boolean start){
    if(start){
      lockTimerStop.start();
      lockTimerTop.start();
      this.isLocked = true;
    }else{
      if(this.isLocked && (lockTimerStop.isFinished() || lockTimerTop.isFinished())){
        Logger.Log("Piece Placed");
        this.isLocked = false;
        this.isPlaced = true;
        return true;
      }else{
        lockTimerStop.start();
      }
    }
    return false;
  }
  void UnLock(){
    this.isLocked = false;
    this.isPlaced = false;
    lockTimerStop = new Timer(lockStop);
    lockTimerTop = new Timer(lockTop);
  }
  
  void Pause(){
    lockTimerStop.Pause();
    lockTimerTop.Pause();
  }
  void Continue(){
    lockTimerStop.start();
    lockTimerTop.start();
  }
  void Print(){
    String line = "";
    boolean[][] stateMatrix = this.getState();
    println("START PIECE");
    for(int j= 0 ; j < stateMatrix[0].length;j++){
      for(int i=0 ; i < stateMatrix.length; i++){
        line += ((stateMatrix[i][j])?type:'0');
      }
      println(line);
      line = "";
    }
    println("END PIECE");
  }
}
