// Tetrodomo
// Created by Imobach Martín 
// kabomi.es
class Board{
  GameSettings Settings;
  public char[][] Matrix = null;
  
  Board(GameSettings s){
    this.Settings = s;
    this.Matrix = new char[this.Settings.numColumns][this.Settings.numRows];
  }
  
  void Save(Piece p){
    Draw(p,true, Integer.MIN_VALUE);
  }
  void Draw(Piece p){
    Draw(p, Integer.MIN_VALUE);
  }
  void Draw(Piece p, int gy){
    Draw(p,false, gy);
  }
  void Draw(Piece p, boolean store, int gy){
    int boundX = -(p.sizex - 1);
    int boundY = -(p.sizey - 1);
    assert p.x >= boundX && p.y >= boundY;
    boolean validGhost = (p.ghost && gy != Integer.MIN_VALUE);
    if( validGhost && gy < -1){
      assert gy >= boundY;
    }
    assert p.x < Settings.numColumns && p.y < Settings.numRows;
    int mx = 0, my = 0, mgy = 0;
    if(p.x < 0) 
      mx = -(p.x);
    if(validGhost && gy < 0)
      mgy = -(gy);
    if(p.y < 0) 
      my = -(p.y);
    
    if(p != null){
      //Introducimos la pieza donde corresponda
      insertPiece(p,p.y,mx,my);
      //Si tiene ghost tambien
      if(validGhost && gy > boundY){
        insertPiece(p,gy,mx,mgy);
      }
    }
    drawBoard(p,mx,my,gy,mgy);
    //Eliminamos el rastro de la pieza
    if(p != null && !store){
      deletePiece(p,p.y,mx,my);
    }
    //Si tiene ghost tambien
    if(validGhost && gy > boundY){
      deletePiece(p,gy,mx,mgy);
    }
  }
  void deletePiece(Piece p, int y, int mx, int my){
    manipulatePieceWithValue(p,y,mx,my,_EMPTY_CHAR);
  }
  void insertPiece(Piece p, int y, int mx, int my){
    manipulatePieceWithValue(p,y,mx,my,p.type);
  }
  void manipulatePieceWithValue(Piece p, int y, int mx, int my, char value){
    int x = p.x;
    boolean[][] pMatrix = p.getState();
    for(int i= mx;i< p.sizex; i++){
      for(int j= my;j< p.sizey; j++){
        if(pMatrix[i][j]){
          try{
            Matrix[x + i][y + j] = value;
          }catch(Exception ex){
            println("Matrix.length:[" + Matrix.length + "][" + Matrix[0].length + "]");
            println("Piece(" + x + ", " + y + ")(mx:" + mx + ", my:" + my + ")");
            println("Boundaries(i,j): (" + p.sizex + "," + p.sizey + ") Indexes(i,j): (" + i + "," + j + ") Query[" + (x + i) + "][" + (y + j) + "] PieceType:" + value);
            Logger.active = true;
            Print();
            Matrix[x + i][y + j] = value;
          }
        }
      }
    }
  }
  void drawBoard(Piece p, int mx, int my, int gy, int mgy){
    boolean[][] pMatrix = p.getState();
    Color pColor;

    for(int col=0;col< this.Settings.numColumns;col++){
      for(int row=0;row< this.Settings.numRows;row++){
        if(this.Matrix[col][row] != _EMPTY_CHAR){
          int shapeType = _SHAPETYPE_RECT_IMG;
          
          if(isCellFilledWithPiece(col, row, p, p.y, mx, my)){
            drawPieceCell(col, row, p.shapeType);
          }else{
            if(isCellFilledWithPiece(col, row, p, gy, mx, mgy)){
              drawGhostCell(col, row);
            }else{
              drawBoardCell(col, row);
            }
          }
        }else{
          drawBoardCell(col, row);
        }
      }
    }
  }
  boolean isCellFilledWithPiece(int col, int row, Piece p, int y, int mx, int my){
    int x = p.x;
    return col >= (x + mx) && 
           col < (x + p.sizex) &&
           row >= (y + my) && 
           row < (y + p.sizey) && 
           p.getState()[col - x][row - y];
  }
  void drawPieceCell(int col, int row, int shapeType){
    drawCell(shapeType, TRANSPARENCY_CELL_PIECE, col, row);
  }
  void drawGhostCell(int col, int row){
    drawCell(_GHOST_SHAPETYPE, TRANSPARENCY_CELL_GHOST, col, row);       
  }
  void drawBoardCell(int col, int row){
    drawCell(Settings.shapeType,TRANSPARENCY_CELL_BOARD, col, row);
  }
  void drawCell(int shapeType, int transparency, int x, int y){
    int cellWidth = this.Settings.cellWidth + this.Settings.borderLR;
    int cellHeight = this.Settings.cellHeight + this.Settings.borderUB;
   
    strokeWeight(0.f);
    switch(shapeType){
      case _SHAPETYPE_RECT_IMG:
        Rect refRect = Rect.Rect(x*cellWidth + this.Settings.borderLR, y*cellHeight + this.Settings.borderUB,
                                 this.Settings.cellWidth, this.Settings.cellHeight);
        int index = -1;
        for(int i=0;i < Settings.pieceTypes.length;i++){
          if(this.Matrix[x][y] == Settings.pieceTypes[i]){
            index = i;
            break;
          }
        }
        if(index == -1){ 
          //drawImage(loadingBackImage, refRect);
        }else
          drawImage(Settings.squareImg[index], refRect);
        break;
      case _SHAPETYPE_RECT:
        Color pColor = new Color(this.Matrix[x][y], transparency);
        fill(pColor.r,pColor.g,pColor.b,pColor.a);
        rect(x*cellWidth,y*cellHeight,cellWidth,cellHeight);
        break;
      case _SHAPETYPE_ELLIPSE:
        ellipse(x*cellWidth + (cellWidth/2)  , y*cellHeight + (cellHeight/2), cellWidth, cellHeight);
        break;
    }
    strokeWeight(1.f);
  }
  
  void Print(){
    if(Logger.active){
      MatrixManager charMatrix = MatrixManager.CharMatrix(this.Matrix);
      charMatrix.Print();
    }
  }
  
  String getFullRows(){
    MatrixManager charMatrix = MatrixManager.CharMatrix(this.Matrix);
    return charMatrix.getFullRows();
  }
  
  void manipulateGivenRows(String rows, int op){
    if(op == _DELETE){
      MatrixManager charMatrix = MatrixManager.CharMatrix(this.Matrix);
      charMatrix.deleteGivenRows(rows);
      this.Matrix = charMatrix.Matrix;
    }
  }
}

