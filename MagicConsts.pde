static final int _DELETE = 0;
static final int _INSERT = 1;
static final int _MOVE_UP = 2;
static final int _MOVE_DOWN = 3;

static final int _NUM_SHAPETYPES_FROM_ZERO = 2; //0,1,2
static final int _SHAPETYPE_RECT_IMG = 0;
static final int _SHAPETYPE_RECT = 1;
static final int _SHAPETYPE_ELLIPSE = 2;

static final int _GHOST_SHAPETYPE = 1;

static final int _ACTION_UP = 1;
static final int _ACTION_DOWN = 0;
static final int _ACTION_MOVE = 2;


static final char _LEFT = 'L';
static final char _RIGHT = 'R';
static final char _DOWN = 'B';
static final char _ROTATE = 'r';
static final char _LEVEL_UP = '+';
static final char _LEVEL_DOWN = '-';
static final char _PIECE_DOWN = ' ';
static final char _PIECE_RESET = '.';
static final char _MUSIC = 'm';
static final char _CONTROLS_VIEW = 'o';

static final char _NEW_GAME = '1';
static final char _LOAD_GAME = '2';
static final char _SAVE_GAME = '3';
static final char _OPTIONS = '4';
static final char _STATS = '5';
static final char _CREDITS = '6';
static final char _EXIT = '7';

static final char _NO_BUTTON_PRESSED = ' ';

static final char _SHAPE_RECT = 'A';
static final char _SHAPE_S = 'B';
static final char _SHAPE_Z = 'C';
static final char _SHAPE_BAR = 'D';
static final char _SHAPE_TRIANGLE = 'E';
static final char _SHAPE_P = 'F';
static final char _SHAPE_L = 'G';
static final char _SHAPE_TROMINO_BOOMERANG = 'H';
static final char _SHAPE_TROMINO_I = 'I';
static final char _SHAPE_DOMINO = 'J';
static final char _SHAPE_MONOMINO = 'K';

static final boolean _COLLISION = false;
static final boolean _NO_COLLISION = true;

static final int _BORDER_LEFT = 0;
static final int _BORDER_UP = 1;
static final int _BORDER_RIGHT = 2;
static final int _BORDER_DOWN = 3;

static final int _COLUMNS_PAD = 2;
static final int _ROWS_PAD = 2;

static final char _TRUE_CHAR = char(1);
static final char _FALSE_CHAR = char(0);
static final char _EMPTY_CHAR = _FALSE_CHAR;
static final char _COLLISION_CHAR = _TRUE_CHAR;
static final char _INSERT_CHAR = _TRUE_CHAR;
static final char _BUTTON_CHAR = _FALSE_CHAR;
static final char _COLOR_WHITE = _FALSE_CHAR;

static final int _BUTTONS_TRANSPARENCY_LENGTH_MS = 300;
static final int _MENU_BUTTONS_PRESSED_LENGTH_MS = 1000;
static final int _MENU_CREDITS_LINK_LENGTH_MS = 5000;

static final int _GESTURE_MOVE_LENGTH_MS = 100;
static final int _GESTURE_TAP_LENGTH_MS = 200;
static final int _GESTURE_DOUBLETAP_LENGTH_MS = 200;
static final int _GESTURE_LONGPRESS_LENGTH_MS = 300;
static final int _GESTURE_FULLSWING_DOWN_LENGTH_MS = 100;

static final char _GAME_MODE_TETRODOMO = '1';
static final char _GAME_MODE_NORMAL = '2';
static final char _GAME_MODE_EXTREME = '3';
static final char _GAME_MODE_EASY_SPIN = '4';

static final int _OPACITY_66 = 66;
static final int _TRANSPARENT = 0;
static final int _ALMOST_TRANSPARENT = 11;
static final int _BIT_TRANSPARENT = 17;
static final int _SEMI_TRANSPARENT = 64;

static final int _WHITE = 255;
static final int _GRAY = 230;
static final int _BLACK = 0;

static final char _HIDE_MENU_SECOND_LEVEL = _FALSE_CHAR;
static final char _SHOW_MENU_SECOND_LEVEL = _TRUE_CHAR;

static final int _MUSIC_BUTTON_INDEX = 4;
static final int _MUSIC_STATE_NO = 0;
static final int _MUSIC_STATE_1 = 1;
static final int _MUSIC_STATE_2 = 2;

static final int _CONTROLS_BUTTON_INDEX = 5;
static final int _CONTROLS_HIDE = 0;
static final int _CONTROLS_SHOW = 1;
