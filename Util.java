import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.lang.reflect.Field;
import JSONorg.*;

public class Util {  
  public static boolean isAndroid(){
    /*System.out.println(java.lang.System.getProperty("java.vendor.url"));//=http://www.android.com/
    System.out.println(java.lang.System.getProperty("java.vm.vendor.url"));//=http://www.android.com/
    System.out.println(java.lang.System.getProperty("java.home"));//=/system
    System.out.println(java.lang.System.getProperty("java.vm.name"));//=Dalvik
    System.out.println(java.lang.System.getProperty("java.runtime.name"));//=Android Runtime
    System.out.println(java.lang.System.getProperty("java.specification.vendor"));//=The Android Project
    System.out.println(java.lang.System.getProperty("java.vm.specification.vendor"));//=The Android Project
    System.out.println(java.lang.System.getProperty("java.vm.vendor"));//=The Android Project
    System.out.println(java.lang.System.getProperty("android.vm.dexfile"));//=true
    System.out.println(java.lang.System.getProperty("java.specification.name"));//=Dalvik Core Library
    System.out.println(java.lang.System.getProperty("java.vendor"));//=The Android Project
    System.out.println(java.lang.System.getProperty("java.vm.specification.name"));//=Dalvik Virtual Machine Specification*/
    return (java.lang.System.getProperty("java.specification.vendor").indexOf("Android") > -1) ||
           (java.lang.System.getProperty("java.vendor").indexOf("Android") > -1) ||
           (java.lang.System.getProperty("java.runtime.name").indexOf("Android") > -1);
  }
  public static Object jsonToObj(JSONObject json, Object obj){
      System.out.println("jsonToObj");
      String[] names = JSONObject.getNames(obj);
      Class<?> c = obj.getClass();
      System.out.println("num names:" +  names.length);
      for(int i=0;i< names.length;i++){
        try{
        Field field = c.getDeclaredField(names[i]);
        if(field.getType().isArray())
          field.set(obj, json.getJSONArray(names[i]));
        else{
          if(field.getType().toString().equals("char"))
            field.set(obj, json.getString(names[i]).charAt(0));
          else
            field.set(obj, json.get(names[i]));
        }
          
        System.out.println(names[i] + ":" + field.getType().toString());
        }catch(Exception ex){
          System.out.println("msg:" + ex.getMessage() + ":trace:");
          ex.printStackTrace(System.out);
        }
      }
      return obj;
    }
  public static JSONObject objToJSON(Object obj){
      JSONObject json = new JSONObject();
      System.out.println("objToJSON");
      String[] names = JSONObject.getNames(obj);
      Class<?> c = obj.getClass();
      System.out.println("num names:" +  names.length);
      for(int i=0;i< names.length;i++){
        try{
        Field field = c.getDeclaredField(names[i]);
        json.put(names[i], field.get(obj));
        System.out.println(names[i]);
        }catch(Exception ex){
          System.out.println("msg:" + ex.getMessage() + ":trace:");
          ex.printStackTrace(System.out);
        }
      }
      return json;
    }
    public static void createDirsFor(String fileName){      
      File file = new File(fileName);
      file = file.getParentFile();
      if(file.mkdirs()){
        System.out.println("Directories created for " + fileName);
      }
    }
    public static void SaveAll(String fileName, String data){
      try {
            File file = new File(fileName);
            createDirsFor(fileName);
            BufferedWriter bw = null;
            if(!file.exists())
                file.createNewFile();
            try {
                bw = new BufferedWriter(new FileWriter(file));
                bw.write(data);
            } catch (IOException ex) {
                System.out.println(ex.getMessage());
            }finally{
                if (bw!=null)
                    bw.close();
            }
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }
    public static int[] jsonArrayToInt(JSONArray obj){
      int[] result =null;
      try{
        result = new int[obj.length()];
        for(int i=0; i< obj.length();i++){
          result[i] = obj.getInt(i);
        }
      }catch(Exception ex){
        System.out.println("msg:" + ex.getMessage() + ":trace:");
        ex.printStackTrace(System.out);
      }
      return result;
    }
    public static float[] jsonArrayToFloat(JSONArray obj){
      float[] result =null;
      try{
        result = new float[obj.length()];
        for(int i=0; i< obj.length();i++){
          result[i] = ((Double) obj.get(i)).floatValue();
        }
      }catch(Exception ex){
        System.out.println("msg:" + ex.getMessage() + ":trace:");
        ex.printStackTrace(System.out);
      }
      return result;
    }
    public static char[] jsonArrayToChar(JSONArray obj){
      char[] result =null;
      try{
        result = new char[obj.length()];
        for(int i=0; i< obj.length();i++){
          result[i] = obj.getString(i).charAt(0);
        }
      }catch(Exception ex){
        System.out.println("msg:" + ex.getMessage() + ":trace:");
        ex.printStackTrace(System.out);
      }
      return result;
    }
    public static char[][] jsonArrayToCharMatrix(JSONArray obj){
      char[][] result =null;
      try{
        result = new char[obj.length()][];
        for(int i=0; i< obj.length();i++){
          result[i] = jsonArrayToChar(obj.getJSONArray(i));
        }
      }catch(Exception ex){
        System.out.println("msg:" + ex.getMessage() + ":trace:");
        ex.printStackTrace(System.out);
      }
      return result;
    }
    public static boolean[] jsonArrayToBoolean(JSONArray obj){
      boolean[] result =null;
      try{
        result = new boolean[obj.length()];
        for(int i=0; i< obj.length();i++){
          result[i] = obj.getBoolean(i);
        }
      }catch(Exception ex){
        System.out.println("msg:" + ex.getMessage() + ":trace:");
        ex.printStackTrace(System.out);
      }
      return result;
    }
    public static boolean[][] jsonArrayToBooleanMatrix(JSONArray obj){
      boolean[][] result =null;
      try{
        result = new boolean[obj.length()][];
        for(int i=0; i< obj.length();i++){
          result[i] = jsonArrayToBoolean(obj.getJSONArray(i));
        }
      }catch(Exception ex){
        System.out.println("msg:" + ex.getMessage() + ":trace:");
        ex.printStackTrace(System.out);
      }
      return result;
    }
}
