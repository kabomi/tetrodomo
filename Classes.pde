public static class Logger{
  static boolean active;
  static void enable(){
    active = true;
  }
  static void disable(){
    active = false;
  }
  static void Log(String msg){
    if(active){
      println(msg);
    }
  }
  static void Log(Exception ex){
    if(active){
      ex.printStackTrace(System.out);
    }
  }
}

public static class AdaptPx{
  static float density;
  static void setDensity(float d){
    density = d;
  }
  static int accordingToDp(int px){
    return floor(px * density);
  } 
}
public class Stat{
  public int points=0;//55555555;
  public int level=0;//55;
  public int totalRows=0;//55555;
  public String date="2000/01/01 00:00";//"2025/05/05 05:25";
  Stat(){
  }
  Stat(int points, int level, int totalRows, String date){
    this.points = points;
    this.level = level;
    this.totalRows = totalRows;
    this.date = date;
  }
}
