static class MatrixManager{
  public char[][] Matrix = null;
  private boolean hasBorder = false;
  
  static MatrixManager CharMatrix(char[][] matrix){
    return new MatrixManager(matrix);
  }
  static MatrixManager CharMatrixWithBorder(char[][] matrix){
    MatrixManager mm = new MatrixManager(matrix);
    mm.hasBorder = true;
    return mm;
  }
  MatrixManager(char[][] matrix){
    this.Matrix = matrix;
  }
  void trimRows(int numRows){
    char[][] newMatrix = new char[Matrix.length][Matrix[0].length - numRows];
    for(int row = numRows; row < Matrix[0].length;row++){
      for(int col= 0; col < Matrix.length; col++){
        newMatrix[col][row - numRows] = Matrix[col][row];
      }
    }
    Matrix = newMatrix;
  }
  void insertGivenRows(String rows){
    manipulateGivenRows(rows, _INSERT);
  }
  void deleteGivenRows(String rows){
    manipulateGivenRows(rows, _DELETE);
  }
  void manipulateGivenRows(String rows, int op){
    int numAffectedRows = 1;
    int[] theRows = int(split(rows,'|'));
    int posInitRow = theRows[0];
    int lastRow = posInitRow;
    for(int i = 1; i< theRows.length; i++){
      if((theRows[i] - lastRow)  != 1){
        manipulateGroupedRows(posInitRow, numAffectedRows, op);
        posInitRow = theRows[i];
        numAffectedRows = 0;
      }
      lastRow = theRows[i];
      numAffectedRows++;
    }
    if(numAffectedRows > 0){
      manipulateGroupedRows(posInitRow, numAffectedRows, op);
    }
  }
  void manipulateGroupedRows(int posInitRow, int numAffectedRows, int op){
    int posLastRow = posInitRow + numAffectedRows - 1;
    String opLogText = "";
    //assert posInitRow >= 0 && posLastRow < this.numRows;
    switch(op){
      case _DELETE:
        opLogText = "Delete";
        deleteGroupedRows(posLastRow, numAffectedRows);
        Logger.Log("resetFirstRows:" + numAffectedRows);
        resetFirstRows(numAffectedRows);
        break;
      case _INSERT:
        opLogText = "Insert";
        insertGroupedRows(posLastRow, numAffectedRows);
        break;
      case _MOVE_UP:
        opLogText = "MoveUp";
        break;
      case _MOVE_DOWN:
        opLogText = "MoveDown";
        break;
    }
    Logger.Log(opLogText + " posInitRow:" + posInitRow + ", numAffectedRows:" + numAffectedRows);
  }
  private void deleteGroupedRows(int posLastRow, int numAffectedRows){
    int initCol = 0;
    int endCol = Matrix.length;
    if(this.hasBorder){
      posLastRow++;
      initCol++;
      endCol--;
    }
    for(int row = posLastRow; row >= numAffectedRows; row--){
      Logger.Log("ROW:" + row + "<-- " + (row - numAffectedRows));
      for(int col = initCol; col < endCol; col++){
        Matrix[col][row] = Matrix[col][row - numAffectedRows];
      }
    }
  }
  private void resetFirstRows(int numberOfRows){
    int initCol = 0;
    int initRow = 0;
    int endCol = Matrix.length;
    if(this.hasBorder){
      initCol++;
      initRow++;
      endCol--;
    }
    for(int row = initRow; row < initRow + numberOfRows;row++){
      Logger.Log("RESET ROW:" + row);
      for(int col= initCol; col < endCol; col++){
        Matrix[col][row] = _EMPTY_CHAR;
      }
    }
  }
  private void insertGroupedRows(int posLastRow, int numAffectedRows){
    for(int j = posLastRow; j > posLastRow - numAffectedRows; j--){
      for(int i = 0; i < Matrix.length; i++){
        Matrix[i][j] = _INSERT_CHAR;
      }
    }
    for(int j = 0; j < posLastRow;j++){
      for(int i= 0; i < Matrix.length; i++){
        Matrix[i][j] = Matrix[i][j + 1];
      }
    }
  }
  String getFullRows(){
    assert Matrix != null;
    String rows = "";
    Boolean found = false;
    for(int j= 0;j< Matrix[0].length;j++){
      found = false;
      for(int i= 0;i < Matrix.length;i++){
        if(Matrix[i][j] == _EMPTY_CHAR){
          found = true;
          break;
        }
      }
      if(!found){
        rows += "|" + j ;
      }
    }
    if(rows.length() > 0){
      rows = rows.substring(1);
    }
    return rows;
  }
  void Print(){
    String line = "";
    for(int j= 0 ; j < Matrix[0].length;j++){
      for(int i=0 ; i < Matrix.length; i++){
        char value = ((Matrix[i][j] == _TRUE_CHAR)?'1':((Matrix[i][j] == _FALSE_CHAR)?'0':Matrix[i][j]));
        line += value;
      }
      println(line);
      line = "";
    }
  }
}
