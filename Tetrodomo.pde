// Tetrodomo
// Created by Imobach Martín 
// kabomi.es

Timer splashTimer = null;
PImage backgroundImage = null;
Timer loadingTimer = null;
int loadingCount = 0;
PImage loadingImage = null;
PImage loadingBackImage = null;
Game Game = null;
MediaPlayer Sound = null;
MediaPlayer Effects = null;
Menu Menu = null;
char GameMode = _GAME_MODE_NORMAL;

PFont mainFont;
PFont loadingFont;

boolean initialized = false;
boolean setupDone = false;

void setup()
{
  Logger.active = _DEBUG;
  
  if(Util.isAndroid()){
    println("ANDROID DETECTED");
    //setPortraitOrientation();
    _SCREEN.width = screenWidth;
    _SCREEN.height = screenHeight;
    Scale.allConstants();
    size(screenWidth, screenHeight, P3D);
  }else{
    try{
      println("NO ANDROID DETECTED");
      JSONorg.JSONObject json = null;
      json = new JSONorg.JSONObject(Utils.ReadAll(dataPath(FILE_JSON_SCREEN + FILETYPE_JSON)));
      _SCREEN.width = json.getInt("width");
      _SCREEN.height = json.getInt("height");
    }catch(Exception ex){
      _SCREEN.width = 480;
      _SCREEN.height = 800;
    }
    Scale.allConstants();
    size(_SCREEN.width, _SCREEN.height, P3D);
  }
  
  setupSplash();

  frameRate(FRAME_RATE);
  strokeWeight(LINE_STROKE_WEIGHT);
  if(RUN_SMOOTHLY)
    smooth();
    
  mainFont = createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE);
  textFont(mainFont);
  
  
  Sound = new MediaPlayer();
  Sound.setup(this, FILE_MAIN_MUSIC);
  Effects = new MediaPlayer();
  Effects.setLooping(false);
  Effects.setup(this);
  Menu = new Menu();
  setupDone = true;
}
void setupSplash(){
  loadingFont = createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_LOADING);
  
  backgroundImage = loadImage(FILE_IMG_LOGO_2);
  
  loadingImage = loadImage(FILE_IMG_LOADING_SHAPE);
  loadingBackImage = loadImage(FILE_IMG_LOADING_BACK_SHAPE);
  loadingCount = 0;
  
  splashTimer = new Timer(SPLASH_IMAGE_LENGHT_MS);
  splashTimer.start();
  
  loadingTimer = new Timer(LOADING_IMAGE_LENGHT_MS);
  loadingTimer.start();
}

void Init(){
  Game = new Game(GameMode);
  Logger.Log("Nuevo Juego");
  //mSound.playFromStart();
  initialized = true;
}

void draw(){
  translate(Scale.translateX, Scale.translateY);
  
  if(splashTimer.isFinished()){
    if(!initialized){
      Menu.Draw();
    }else{
      if(Menu.isVisible){
        Menu.Draw();
      }else{
        Game.Draw();
      }
    }
    /*if(!player.isPlaying()){
      loopPosition = 0;
      player.seekTo(loopPosition);
      player.play();
    }*/
  }else{
    drawLoader();
  }
}
void drawLoader(){
  background(BACKGROUND_R, BACKGROUND_G, BACKGROUND_B);
  drawImage(backgroundImage, LOADING_IMG_LOGO_RECT);

  if(loadingTimer.isFinished()){
    loadingTimer = new Timer(LOADING_IMAGE_LENGHT_MS);
    loadingTimer.start();
    loadingCount++;
  }
  
  fill(255);
  drawText(loadingFont, LOADING_TEXT, LOADING_TEXT_RECT);

  scale(Scale.scaleWidth, Scale.scaleHeight);

  Rect piecesRect = LOADING_PIECES_RECT.copy();
  for(int i=0; i < 10;i++){
    drawImage((loadingCount < i?loadingBackImage:loadingImage), piecesRect);
    piecesRect.moveX(piecesRect.width + 2);
  }
}

void drawImage(PImage img, Rect rect){
  image(img, rect.x, rect.y, rect.width, rect.height);
}

void drawText(PFont font, String txt, Rect rect){
  textFont(font);
  drawText(txt, rect);
}
void drawText(String txt, Rect rect){
  text(txt, rect.x, rect.y, rect.width, rect.height);
}

void SetGameMode(char mode){
  GameMode = mode;
}

void loadStats(){
  Menu.loadStatsData();
}

void Restart(){
  noLoop();
  Logger.Log("PreInit");
  Init();
  Resume();
  Logger.Log("PosInit");
  loop();
}

void Pause(){
  if(initialized){ 
    Sound.pause();
    Game.Pause();
    if(Game.gameOver){
      Menu.inputEvent(_HIDE_MENU_SECOND_LEVEL);
      Menu.show();
      loop();
    }
  }
  if(setupDone){
    Effects.playMediaFile(FILE_EFFECT_PAUSE);
    Menu.show();
  }
}

void Resume(){
  if(initialized && !Game.gameOver){
    if(Game.shouldMusicBePlaying())
      Sound.play();
    Game.Resume();
    Menu.hide();
  }
}

void SaveGame(){
  if(!initialized)
    return;
  Logger.Log("saving game");
  noLoop();
  Game.Save();
  Resume();
  loop();
  Logger.Log("game saved");
}

void LoadGame(){
  Logger.Log("loading game");
  noLoop();
  Init();
  Game.Load();
  Resume();
  loop();
  Logger.Log("game loaded");
}

void changeMusic(int option){
  switch(option){
    case _MUSIC_STATE_NO:
      Sound.pause();
      println("No music");
      break;
    case _MUSIC_STATE_1:
      Sound.setup(this, FILE_MAIN_MUSIC);
      if(!Menu.isVisible)
        Sound.playFromStart();
      println("Play " + FILE_MAIN_MUSIC);
      break;
    case _MUSIC_STATE_2:
      Sound.setup(this, FILE_MAIN_MUSIC_2);
      if(!Menu.isVisible)
        Sound.playFromStart();
      println("Play " + FILE_MAIN_MUSIC_2);
      break;
  }
}

void Exit(){
  exit();
}

void stop()
{
  println("Stop");
  if(setupDone){
    Sound.release();
    Effects.release();
  }
  // this calls the stop method that 
  // you are overriding by defining your own
  // it must be called so that your application 
  // can do all the cleanup it would normally do
  super.stop();
}
