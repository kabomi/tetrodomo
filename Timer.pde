// Learning Processing
// Daniel Shiffman
// http://www.learningprocessing.com

// Example 10-5: Object-oriented timer
// Modified by kabomi.es

class Timer {
 
  int savedTime; // When Timer started
  int totalTime; // How long Timer should last
  int elapsedTime; //Tiempo acumulado
  boolean isPaused;
  
  Timer(){
    savedTime = 0;
    totalTime = -1;
    elapsedTime = 0;
    isPaused = false;
  }
  Timer(int tempTotalTime) {
    savedTime = 0;
    totalTime = tempTotalTime;
    elapsedTime = 0;
    isPaused = false;
  }
  
  void start() {
    // When the timer starts it stores the current time in milliseconds.
    int passedTime = millis()- savedTime;
    elapsedTime += passedTime;
    savedTime = millis(); 
    isPaused = false;
  }
  
  boolean isFinished() { 
    // Check how much time has passed
    int passedTime = millis()- savedTime;
    if (passedTime > totalTime) {
      return true;
    } else {
      return false;
    }
  }

  void Pause(){
    if(!isPaused){
      int passedTime = millis()- savedTime;
      elapsedTime += passedTime;
      isPaused = true;
    }
  }
  void Continue(){
    savedTime = millis(); 
    isPaused = false;
  }
  int getElapsedTime(){
    int passedTime = millis()- savedTime;
    elapsedTime += passedTime;
    return elapsedTime;
  }
  
}
