/*There are constants without final attrib because processing
  works with pixels and not with dp, so some constants need to
  be scaled */
static final boolean _DEBUG = false;

static       Dim _SCREEN = Dim.Dim(320, 480);
static final Dim _BASE_SCREEN = Dim.Dim(320, 480);

static final int     FRAME_RATE = 25;
static final boolean RUN_SMOOTHLY = false;
static final int     LINE_STROKE_WEIGHT = 0;
static final int     MAX_EVENTS_BUFFER = 10;
static final int     MAX_MENU_EVENTS_BUFFER = 1;

static final String AUTHOR_LINK = "http://kabomi.es";

/*Images*/
//static final String FILE_IMG_SPLASH = "splash-screen-320x480.png";
//static final String FILE_IMG_GAME_BACKGROUND = "game-background-320x480.png";
static final String FILE_IMG_LOADING_SHAPE = "squareLoadingHQG.png";//"cuadrado-20x20.png";
static final String FILE_IMG_LOADING_BACK_SHAPE = "squareLoadingHQ.png";//"cuadrado-20x20.png";
//static final String FILE_IMG_CREDITS = "credits.png";
static final String FILE_IMG_LOGO_2 = "logo_pantalla2HQ.png";
static final String FILE_IMG_MENU = "fondo2_pantalla2HQ.png";
//static final String FILE_IMG_MENU_BACKGROUND = "fondo2_pantalla2HQ.png";//"background-menu-320x480.png";
static final String FILE_IMG_MENU_BUTTON_SHAPE_2 = "squareMenuK.png";
static final String FILE_IMG_MENU_BUTTON_SHAPE = "squareMenu.png";
static final String FILE_IMG_MENU_STATS_LOGO = "logo_pantalla_STATS.png";
static final String FILE_IMG_MENU_STATS_STRIPE = "banda_azul.png";
static final String FILE_IMG_MENU_STATS_SQUARE = "squareStats.png";
static final String FILE_IMG_MENU_STATS_RECTANGLE = "rectangleStats.png";
static final String FILE_IMG_LOGO_3 = "logo_pantalla3HQ.png";
static final String FILE_IMG_BOARD = "fondo3_grid_grande.png";
//static final String FILE_IMG_GAMESETTINGS_SHAPE = "square.png";
//static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTONS = "Botones-violeta.png";
static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_RIGHT_ARROW = "control_right.png";
static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_LEFT_ARROW = "control_left.png";
static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_DOWN_ARROW = "control_down.png";
static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_REFRESH = "control_rotate.png";
/*static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_RIGHT_ARROW = "arrow32-right.png";
static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_LEFT_ARROW = "arrow32-left.png";
static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_DOWN_ARROW = "arrow32-down.png";
static final String FILE_IMG_GAMESETTINGS_SHAPE_BUTTON_REFRESH = "refresh.png";*/
static final String FILE_IMG_GAME_NO_MUSIC = "music-no.png";
static final String FILE_IMG_GAME_MUSIC_1 = "music-1.png";
static final String FILE_IMG_GAME_MUSIC_2 = "music-2.png";
static final String FILE_IMG_GAME_HIDE_CONTROLS = "controls-hide.png";
static final String FILE_IMG_GAME_SHOW_CONTROLS = "controls-show.png";
static final String FILE_IMG_CC = "chooser_cc_hover.png";
static final String FILENAMEPREFIX_IMG_PIECES ="piezaHQ";//"Tetromino_";
static final String FILENAMEPREFIX_IMG_SQUARE = "squareHQ";
static final String FILENAMEPREFIX_IMG_MENU_SQUARE = "squareMenu";
static final String FILETYPE_PNG =".png";
static final String FILETYPE_SVG =".svg";
/*End*/
/*Sounds*/
static final String FILE_MAIN_MUSIC = "TDeeJay_-_DisTOrATion.ogg";//"The_Extreme_-_Sooner_or_Later_(The_Extreme_Chillout_Remix).ogg";//"TDeeJay_-_DisTOrATion.ogg";
static final String FILE_MAIN_MUSIC_2 = "The_Extreme_-_Sooner_or_Later_(The_Extreme_Chillout_Remix).ogg";//"macargentina81_-_chill_out_on_the_seesaw.ogg";//"ChuckBerglund_-_Migration.ogg";//"greg_baumont_-_Chillout_Electric_Piano_Chords.ogg";
static final String FILE_EFFECT_BUTTON = "55845__sergenious__pushbutn.wav";
static final String FILE_EFFECT_PIECE_PLACED = "146718__fins__button.wav";
static final String FILE_EFFECT_LINES = "191754__fins__button-5.wav";
static final String FILE_EFFECT_PAUSE = "146725__fins__laser.wav";
static final String FILE_EFFECT_GAME_OVER = "72866__corsica-s__game-over.wav";
static final String FILE_EFFECT_LEVEL_UP = "187024__lloydevans09__jump2.wav";
/*End*/
/*Other Files*/
static final String FILE_JSON_SCREEN = "screen";
static final String FILE_JSON_SETTINGS = "settings.json";
static final String FILE_JSON_SAVEGAME = "savegame";
static final String FILE_JSON_STATS = "stats";
static final String FILETYPE_JSON = ".json";
/*End*/
/*Background*/
static final int BACKGROUND_R = 189;
static final int BACKGROUND_G = 204;
static final int BACKGROUND_B = 212;
static final int BACKGROUND_GRAY = 230;
/*End*/
/*FONTS*/
static final String FONT_TYPE = "Droid Sans Mono";
static final String FONT_FILE_ARCHITECTSDAUGHTER = "ArchitectsDaughter.ttf";

static int FONT_SIZE = 13;
static int FONT_SIZE_LOADING = 13;
static int FONT_SIZE_MENU_EXIT = 18;
static int FONT_SIZE_MENU_CREDITS = 9;
static int FONT_SIZE_MENU_STATS = 12;
static int FONT_SIZE_MENU_MODE = 11;
static int FONT_SIZE_MENU_NORMAL_MODE = 9;
static int FONT_SIZE_GAME_TITLES = 12;
static int FONT_SIZE_GAME_SCORE = 10;
static int FONT_SIZE_STATS_MODE = 11;
static int FONT_SIZE_STATS_TEXT = 11;
static int FONT_SIZE_STATS_TEXT_DATE = 9;
static int FONT_SIZE_CREDITS_TEXT_LICENSE = 8;

/*END*/
/*Splash Screen*/
static int    SPLASH_IMAGE_LENGHT_MS = 2000;
static int    LOADING_IMAGE_LENGHT_MS = round(SPLASH_IMAGE_LENGHT_MS/11);

static Rect   LOADING_IMG_LOGO_RECT = Rect.Rect(74, 185, 177, 36);
static Rect   LOADING_TEXT_RECT =     Rect.Rect(122, 272, 76, 20); 
static Rect   LOADING_PIECES_RECT =   Rect.Rect(66, 238, 17, 17);
static String LOADING_TEXT = "LOADING...";
/*End*/
/*Menu Screen*/
static Rect MENU_IMG_RECT = Rect.Rect(34, 61, 252, 385);
static Rect MENU_IMG_LOGO_RECT = Rect.Rect(74,34, 172, 36);
//Text
static String MENU_TEXT_NEWGAME = "NEW GAME";
static String MENU_TEXT_LOADGAME = "LOAD GAME";
static String MENU_TEXT_SAVEGAME = "SAVE GAME";
static String MENU_TEXT_STATS = "STATS";
static String MENU_TEXT_OPTIONS = "OPTIONS";
static String MENU_TEXT_CREDITS = "CREDITS";
static String MENU_TEXT_EXIT = "EXIT";

static String MENU_TEXT_TETRODOMO = "TETRO DOMO";
static String MENU_TEXT_EASY_SPIN = "EASY SPIN";
static String MENU_TEXT_NORMAL = "NORMAL";
static String MENU_TEXT_EXTREME = "EX TREME";

static String MENU_TEXT_STATS_LEVEL = "LEVEL";
static String MENU_TEXT_STATS_POINTS = "POINTS";
static String MENU_TEXT_STATS_ROWS = "ROWS";
static String MENU_TEXT_STATS_DATE = "DATE";

static Point MENU_TEXT_BUTTONS_SEPARATION = Point.Point(6,6);
static Dim   MENU_TEXT_BUTTON_DIM = Dim.Dim(48,48);

static Rect MENU_TEXT_NEWGAME_RECT =  Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y     , MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_LOADGAME_RECT = Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y     , MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_SAVEGAME_RECT = Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y     , MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_STATS_RECT =    Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y + 15, MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_CREDITS_RECT =  Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y + 18, MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_EXIT_RECT =     Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y + 12, MENU_TEXT_BUTTON_DIM);

static Rect MENU_TEXT_MODE_TETRODOMO_RECT = Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y + 6 , MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_MODE_EASYSPIN_RECT =  Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y + 6 , MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_MODE_NORMAL_RECT =    Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y + 18, MENU_TEXT_BUTTON_DIM);
static Rect MENU_TEXT_MODE_EXTREME_RECT =   Rect.Rect(MENU_TEXT_BUTTONS_SEPARATION.x, MENU_TEXT_BUTTONS_SEPARATION.y + 6 , MENU_TEXT_BUTTON_DIM);
//
//BUTTONS
char[] MENU_BUTTONS_IMG = new char[]{     _SHAPE_RECT,     _BUTTON_CHAR,    _SHAPE_L, 
                                          _BUTTON_CHAR,    _BUTTON_CHAR,    _SHAPE_L,
                                          _SHAPE_TRIANGLE, _SHAPE_TRIANGLE, _SHAPE_Z,
                                          _SHAPE_TRIANGLE, _BUTTON_CHAR,    _SHAPE_Z,
                                          _BUTTON_CHAR,    _SHAPE_Z,        _BUTTON_CHAR};

char[] MENU_MODE_BUTTONS_IMG = new char[]{_SHAPE_TROMINO_I, _BUTTON_CHAR,    _SHAPE_BAR, 
                                          _SHAPE_TROMINO_I, _BUTTON_CHAR,    _SHAPE_BAR,
                                          _SHAPE_TROMINO_I, _BUTTON_CHAR,    _SHAPE_BAR,
                                          _SHAPE_P        , _BUTTON_CHAR,    _SHAPE_MONOMINO,
                                          _SHAPE_P        , _SHAPE_MONOMINO, _SHAPE_MONOMINO};
                                        
char[] MENU_SQUARE_TYPES = new char[]{_SHAPE_RECT, _SHAPE_Z, _SHAPE_TRIANGLE, _SHAPE_L, _SHAPE_TROMINO_I, _SHAPE_BAR, _SHAPE_P, _SHAPE_MONOMINO};

static Dim   MENU_BUTTON_DIM = Dim.Dim(60,60);
static Point MENU_BUTTON_SEPARATION = Point.Point(65,65);
static Point MENU_BUTTONS_POINT = Point.Point(65,94);

static Rect MENU_BUTTON_NEWGAME_RECT =   Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x  , MENU_BUTTONS_POINT.y                             , MENU_BUTTON_DIM);
static Rect MENU_BUTTON_LOADGAME_RECT =  Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x  , MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y  , MENU_BUTTON_DIM);
static Rect MENU_BUTTON_SAVEGAME_RECT =  Rect.Rect(MENU_BUTTONS_POINT.x                             , MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y  , MENU_BUTTON_DIM);
static Rect MENU_BUTTON_STATS_RECT =     Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x  , MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y*3, MENU_BUTTON_DIM);
static Rect MENU_BUTTON_CREDITS_RECT =   Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x*2, MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y*4, MENU_BUTTON_DIM);
static Rect MENU_BUTTON_EXIT_RECT =      Rect.Rect(MENU_BUTTONS_POINT.x                             , MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y*4, MENU_BUTTON_DIM);

static Rect MENU_BUTTON_MODE_TETRODOMO_RECT = Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x  , MENU_BUTTONS_POINT.y                             , MENU_BUTTON_DIM);
static Rect MENU_BUTTON_MODE_EASYSPIN_RECT =  Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x  , MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y  , MENU_BUTTON_DIM);
static Rect MENU_BUTTON_MODE_NORMAL_RECT =    Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x  , MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y*2, MENU_BUTTON_DIM);
static Rect MENU_BUTTON_MODE_EXTREME_RECT =   Rect.Rect(MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x  , MENU_BUTTONS_POINT.y + MENU_BUTTON_SEPARATION.y*3, MENU_BUTTON_DIM);
//
//STATS
static int   STATS_SAVE_LIMIT = 3;
static Rect  STATS_IMG_LOGO_RECT =   Rect.Rect(107,15, 106, 23);
static Rect  STATS_IMG_STRIPE_RECT = Rect.Rect(0, 0, 320, 50);
static Dim   STATS_IMG_SQUARE_DIM =  Dim.Dim(10,10);
static Rect  STATS_IMG_SQUARE_RECT = Rect.Rect(105, 99, STATS_IMG_SQUARE_DIM);
static Point STATS_IMG_SQUARE_SEPARATION = Point.Point(90 + STATS_IMG_SQUARE_DIM.width, 85 + STATS_IMG_SQUARE_DIM.height);
static Dim   STATS_IMG_RECTANGLE_DIM =  Dim.Dim(284,54);
static Rect  STATS_IMG_RECTANGLE_RECT = Rect.Rect(18, 117, STATS_IMG_RECTANGLE_DIM);
static int   STATS_IMG_RECTANGLE_SEPARATION_Y =  41 + STATS_IMG_RECTANGLE_DIM.height;

static Dim   STATS_TEXT_DIM = Dim.Dim(50,20);//50,10);
static Point STATS_TEXT_POINT = Point.Point(18, 63);
static Rect  STATS_TEXT_LEVEL_RECT =  Rect.Rect(STATS_TEXT_POINT.x                                  , STATS_TEXT_POINT.y, STATS_TEXT_DIM);
static Rect  STATS_TEXT_POINTS_RECT = Rect.Rect(STATS_TEXT_LEVEL_RECT.x  + STATS_TEXT_DIM.width + 27, STATS_TEXT_POINT.y, STATS_TEXT_DIM);
static Rect  STATS_TEXT_ROWS_RECT =   Rect.Rect(STATS_TEXT_POINTS_RECT.x + STATS_TEXT_DIM.width + 30, STATS_TEXT_POINT.y, STATS_TEXT_DIM);
static Rect  STATS_TEXT_DATE_RECT =   Rect.Rect(STATS_TEXT_ROWS_RECT.x   + STATS_TEXT_DIM.width + 27, STATS_TEXT_POINT.y, STATS_TEXT_DIM);
static Dim   STATS_TEXT_MODE_DIM = Dim.Dim(90,20);//50,10);
static Rect  STATS_TEXT_MODE_RECT = Rect.Rect(105 + STATS_IMG_SQUARE_DIM.width, 97, STATS_TEXT_MODE_DIM);

static Point STATS_TEXT_VALUE_POINT =       Point.Point(30, 140);
static Rect  STATS_TEXT_LEVEL_VALUE_RECT =  Rect.Rect(STATS_TEXT_VALUE_POINT.x, STATS_TEXT_VALUE_POINT.y, 30, 30);
static Rect  STATS_TEXT_POINTS_VALUE_RECT = Rect.Rect(STATS_TEXT_LEVEL_VALUE_RECT.x  + STATS_TEXT_LEVEL_VALUE_RECT.width  + 12, STATS_TEXT_VALUE_POINT.y, 73, STATS_TEXT_LEVEL_VALUE_RECT.height);
static Rect  STATS_TEXT_ROWS_VALUE_RECT =   Rect.Rect(STATS_TEXT_POINTS_VALUE_RECT.x + STATS_TEXT_POINTS_VALUE_RECT.width + 16, STATS_TEXT_VALUE_POINT.y, 60, STATS_TEXT_LEVEL_VALUE_RECT.height);
static Rect  STATS_TEXT_DATE_VALUE_RECT =   Rect.Rect(STATS_TEXT_ROWS_VALUE_RECT.x + STATS_TEXT_ROWS_VALUE_RECT.width + 16    , STATS_TEXT_VALUE_POINT.y, 54, STATS_TEXT_LEVEL_VALUE_RECT.height);
static int   STATS_TEXT_VALUE_SEPARATION_Y =  95;
//
/*End*/
/*Game Screen*/
static Rect BOARD_IMG_RECT = Rect.Rect(17,33, 230, 430);
static Rect BOARD_IMG_LOGO_RECT = Rect.Rect(62,17, 140, 30);

//Text
static Dim   GAME_TEXT_DIM = Dim.Dim(100,20);
static Point GAME_TEXT_NUMBERS_SEPARATION = Point.Point(10,15);
static Rect  GAME_TEXT_SCORE_RECT =    Rect.Rect(256, 33, GAME_TEXT_DIM);
static Rect  GAME_TEXT_SCORE_NUMBERS_RECT = Rect.Rect(GAME_TEXT_SCORE_RECT.x + GAME_TEXT_NUMBERS_SEPARATION.x, GAME_TEXT_SCORE_RECT.y + GAME_TEXT_NUMBERS_SEPARATION.y, GAME_TEXT_DIM);
static Rect  GAME_TEXT_LEVEL_RECT =    Rect.Rect(256,72, GAME_TEXT_DIM);
static Rect  GAME_TEXT_LEVEL_NUMBERS_RECT = Rect.Rect(GAME_TEXT_LEVEL_RECT.x + GAME_TEXT_NUMBERS_SEPARATION.x, GAME_TEXT_LEVEL_RECT.y + GAME_TEXT_NUMBERS_SEPARATION.y, GAME_TEXT_DIM);
static Rect  GAME_TEXT_NEXT_RECT =     Rect.Rect(256,111, GAME_TEXT_DIM);
static Rect  GAME_TEXT_GAMEOVER_RECT = Rect.Rect(88,160, GAME_TEXT_DIM);



static String GAME_TEXT_SCORE = "SCORE";
static String GAME_TEXT_NEXT = "NEXT";
static String GAME_TEXT_LEVEL = "LEVEL";
static String GAME_TEXT_GAME_OVER = "GAME OVER";
//
//BUTTONS
static int   BUTTON_BACKGROUND_COLOR = #4A085A;
//static Dim   BUTTONS_SHAPE_DIM =       Dim.Dim(321, 88);
static Dim   BUTTON_IMAGE_ROTATE_SCALE_DIM = Dim.Dim(80, 80);
static Dim   BUTTON_IMAGE_SCALE_DIM =  Dim.Dim(80, 80);
static Dim   BUTTON_DIM =              Dim.Dim(90, 80);
static int   BUTTON_DOWN_WIDTH =       140;

static int  CONTROL_BUTTONS_POINT_Y =  390;
static Rect BUTTON_IMAGE_LEFT_RECT =   Rect.Rect(Point.Point(10                                                           , CONTROL_BUTTONS_POINT_Y)  , BUTTON_IMAGE_SCALE_DIM);
static Rect BUTTON_IMAGE_DOWN_RECT =   Rect.Rect(Point.Point(BUTTON_IMAGE_LEFT_RECT.x + BUTTON_IMAGE_SCALE_DIM.width + 30 , BUTTON_IMAGE_LEFT_RECT.y) , BUTTON_IMAGE_SCALE_DIM);
static Rect BUTTON_IMAGE_RIGHT_RECT =  Rect.Rect(Point.Point(BUTTON_IMAGE_DOWN_RECT.x + BUTTON_IMAGE_SCALE_DIM.width + 30 , BUTTON_IMAGE_LEFT_RECT.y) , BUTTON_IMAGE_SCALE_DIM);
static Rect BUTTON_IMAGE_ROTATE_RECT = Rect.Rect(Point.Point(BUTTON_IMAGE_LEFT_RECT.x, CONTROL_BUTTONS_POINT_Y - BUTTON_IMAGE_SCALE_DIM.height - 30)  , BUTTON_IMAGE_ROTATE_SCALE_DIM);

static int   BUTTON_ROTATE_SEPARATION_Y = CONTROL_BUTTONS_POINT_Y - BUTTON_DIM.height - 30;

static Rect BUTTON_LEFT_RECT =   Rect.Rect(Point.Point(0, CONTROL_BUTTONS_POINT_Y), BUTTON_DIM); 
static Rect BUTTON_DOWN_RECT =   Rect.Rect(Point.Point(BUTTON_DIM.width, CONTROL_BUTTONS_POINT_Y), Dim.Dim(BUTTON_DOWN_WIDTH, BUTTON_IMAGE_SCALE_DIM.height)); 
static Rect BUTTON_RIGHT_RECT =  Rect.Rect(Point.Point(BUTTON_DIM.width + BUTTON_DOWN_WIDTH, CONTROL_BUTTONS_POINT_Y), BUTTON_DIM);
static Rect BUTTON_ROTATE_RECT = Rect.Rect(Point.Point(10, BUTTON_ROTATE_SEPARATION_Y), BUTTON_IMAGE_ROTATE_SCALE_DIM);
//static Dim   BUTTONS_SHAPE_DIM =       Dim.Dim(321, 88);
/*static Dim   BUTTON_IMAGE_ROTATE_SCALE_DIM = Dim.Dim(80, 80);
static Dim   BUTTON_IMAGE_SCALE_DIM =  Dim.Dim(80, 80);
static Dim   BUTTON_DIM =              Dim.Dim(90, 40);
static int   BUTTON_DOWN_WIDTH =       140;

static int  CONTROL_BUTTONS_POINT_Y =  425;
static Rect BUTTON_IMAGE_LEFT_RECT =   Rect.Rect(Point.Point((BUTTON_DIM.width - BUTTON_IMAGE_SCALE_DIM.width)/2                     , CONTROL_BUTTONS_POINT_Y + (BUTTON_DIM.height - BUTTON_IMAGE_SCALE_DIM.height)/2)  , BUTTON_IMAGE_SCALE_DIM);
static Rect BUTTON_IMAGE_DOWN_RECT =   Rect.Rect(Point.Point((BUTTON_DOWN_WIDTH - BUTTON_IMAGE_SCALE_DIM.width)/2 + BUTTON_DIM.width , BUTTON_IMAGE_LEFT_RECT.y + (BUTTON_DIM.height - BUTTON_IMAGE_SCALE_DIM.height)/2) , BUTTON_IMAGE_SCALE_DIM);
static Rect BUTTON_IMAGE_RIGHT_RECT =  Rect.Rect(Point.Point(BUTTON_IMAGE_LEFT_RECT.x + BUTTON_DOWN_WIDTH + BUTTON_DIM.width         , BUTTON_IMAGE_LEFT_RECT.y)                                                         , BUTTON_IMAGE_SCALE_DIM);
static Rect BUTTON_IMAGE_ROTATE_RECT = Rect.Rect(Point.Point(BUTTON_IMAGE_RIGHT_RECT.x                                               , BUTTON_IMAGE_RIGHT_RECT.y - BUTTON_DIM.height)                                    , BUTTON_IMAGE_ROTATE_SCALE_DIM);

static int   BOARD_BUTTONS_SEPARATION_Y = 386;
static int   BUTTON_SEPARATION_Y =        BOARD_BUTTONS_SEPARATION_Y + BUTTON_DIM.height;
static int   BUTTON_ROTATE_SEPARATION_Y = BOARD_BUTTONS_SEPARATION_Y - BUTTON_DIM.height/2;

static Rect BUTTON_LEFT_RECT =   Rect.Rect(Point.Point(0, BUTTON_SEPARATION_Y), Dim.Dim(BUTTON_DIM.width, BUTTON_DIM.height*2)); 
static Rect BUTTON_DOWN_RECT =   Rect.Rect(Point.Point(BUTTON_DIM.width, BUTTON_SEPARATION_Y), Dim.Dim(BUTTON_DOWN_WIDTH, BUTTON_DIM.height*2)); 
static Rect BUTTON_RIGHT_RECT =  Rect.Rect(Point.Point(BUTTON_DIM.width + BUTTON_DOWN_WIDTH, BUTTON_SEPARATION_Y), Dim.Dim(BUTTON_DIM.width, BUTTON_DIM.height*2));
static Rect BUTTON_ROTATE_RECT = Rect.Rect(Point.Point(BUTTON_DIM.width + BUTTON_DOWN_WIDTH, BUTTON_ROTATE_SEPARATION_Y), Dim.Dim(BUTTON_DIM.width, BUTTON_DIM.height*3/2));*/
//
//BOARD
static int   COLUMNS_NUM = 10;
static int   ROWS_NUM = 20;
static Dim   BOARD_CELL_DIM = Dim.Dim(20, 20);
static Dim   BOARD_CELL_DIM_SCALED = BOARD_CELL_DIM;
static int   BOARD_BORDER_UB = 0;
static int   BOARD_BORDER_LR = 0;
static Point BOARD_GRID_SEPARATION = Point.Point(32,48);

static int TRANSPARENCY_CELL_PIECE = 175;
static int TRANSPARENCY_CELL_GHOST = 75;
static int TRANSPARENCY_CELL_BOARD = 175;
//
//Buffer
static int  BUFFER_PIECES_NUM = 3;
static Rect BUFFER_PIECE_RECT = Rect.Rect(266, 126, 37, 37);
//
//Options
static Rect OPTIONS_RECT = Rect.Rect(266, 269, 37, 37);
static int  OPTIONS_SEPARATION_Y = 15;
//Touch
static int SWING_DOWN_TILL_PIECE_DOWN_REACTIVITY = 2;
//
/*End*/



static boolean alreadyScaled = false;
public static class Scale{
  static float scaleWidth = 1.0f;
  static float scaleHeight = 1.0f;
  static int   translateX = 0;
  static int   translateY = 0;
  static Point scalePoint(Point point){
    return Point.Point(Scale.scaleWidth(point.x), Scale.scaleHeight(point.y));
  }
  static Dim scaleDim(Dim dim){
    return Dim.Dim(Scale.scaleWidth(dim.width), Scale.scaleHeight(dim.height));
  }
  static Rect scaleRect(Rect rect){
    return Rect.Rect(Scale.scalePoint(rect.point), Scale.scaleDim(rect.dim));
  }
  static int scaleWidth(int value){
    return round(value*scaleWidth);
  }
  static int scaleHeight(int value){
    return round(value*scaleHeight);
  }
  static void allConstants(){
    if(alreadyScaled)
      return;
    
    scaleWidth =(float(_SCREEN.width)/(_BASE_SCREEN.width));
    scaleHeight = (float(_SCREEN.height)/(_BASE_SCREEN.height));
    if(scaleWidth != scaleHeight){
      if(scaleWidth < scaleHeight){
        translateY = int(float(_BASE_SCREEN.height)* (scaleHeight - scaleWidth) / 2);
        scaleHeight = scaleWidth;
      }else{
        translateX = int(float(_BASE_SCREEN.width)* (scaleWidth - scaleHeight) / 2);
        scaleWidth = scaleHeight;
      }
    }
    
    FONT_SIZE = Scale.scaleWidth(FONT_SIZE);
    FONT_SIZE_LOADING = Scale.scaleWidth(FONT_SIZE_LOADING);
    FONT_SIZE_MENU_CREDITS = Scale.scaleWidth(FONT_SIZE_MENU_CREDITS);
    FONT_SIZE_MENU_EXIT = Scale.scaleWidth(FONT_SIZE_MENU_EXIT);
    FONT_SIZE_MENU_STATS = Scale.scaleWidth(FONT_SIZE_MENU_STATS);
    FONT_SIZE_MENU_MODE = Scale.scaleWidth(FONT_SIZE_MENU_MODE);
    FONT_SIZE_MENU_NORMAL_MODE = Scale.scaleWidth(FONT_SIZE_MENU_NORMAL_MODE);
    FONT_SIZE_GAME_TITLES = Scale.scaleWidth(FONT_SIZE_GAME_TITLES);
    FONT_SIZE_GAME_SCORE = Scale.scaleWidth(FONT_SIZE_GAME_SCORE);
    FONT_SIZE_STATS_MODE = Scale.scaleWidth(FONT_SIZE_STATS_MODE);
    FONT_SIZE_STATS_TEXT = Scale.scaleWidth(FONT_SIZE_STATS_TEXT);
    FONT_SIZE_STATS_TEXT_DATE = Scale.scaleWidth(FONT_SIZE_STATS_TEXT_DATE);
    FONT_SIZE_CREDITS_TEXT_LICENSE = Scale.scaleWidth(FONT_SIZE_CREDITS_TEXT_LICENSE);
    
    LOADING_TEXT_RECT = Scale.scaleRect(LOADING_TEXT_RECT);
    
    GAME_TEXT_SCORE_RECT = Scale.scaleRect(GAME_TEXT_SCORE_RECT);
    GAME_TEXT_SCORE_NUMBERS_RECT = Scale.scaleRect(GAME_TEXT_SCORE_NUMBERS_RECT);
    GAME_TEXT_NEXT_RECT = Scale.scaleRect(GAME_TEXT_NEXT_RECT);
    GAME_TEXT_LEVEL_RECT = Scale.scaleRect(GAME_TEXT_LEVEL_RECT);
    GAME_TEXT_LEVEL_NUMBERS_RECT = Scale.scaleRect(GAME_TEXT_LEVEL_NUMBERS_RECT);
    GAME_TEXT_GAMEOVER_RECT = Scale.scaleRect(GAME_TEXT_GAMEOVER_RECT);
    //GAME_TEXT_DIM = Scale.scaleDim(GAME_TEXT_DIM);
    GAME_TEXT_NUMBERS_SEPARATION = Scale.scalePoint(GAME_TEXT_NUMBERS_SEPARATION);
    
    MENU_TEXT_BUTTONS_SEPARATION = Scale.scalePoint(MENU_TEXT_BUTTONS_SEPARATION);
    MENU_TEXT_BUTTON_DIM = Scale.scaleDim(MENU_TEXT_BUTTON_DIM);
    
    
    MENU_TEXT_NEWGAME_RECT =  Scale.scaleRect(MENU_TEXT_NEWGAME_RECT);
    MENU_TEXT_LOADGAME_RECT = Scale.scaleRect(MENU_TEXT_LOADGAME_RECT);
    MENU_TEXT_SAVEGAME_RECT = Scale.scaleRect(MENU_TEXT_SAVEGAME_RECT);
    MENU_TEXT_STATS_RECT =    Scale.scaleRect(MENU_TEXT_STATS_RECT);
    MENU_TEXT_CREDITS_RECT =  Scale.scaleRect(MENU_TEXT_CREDITS_RECT);
    MENU_TEXT_EXIT_RECT =     Scale.scaleRect(MENU_TEXT_EXIT_RECT);
    
    MENU_TEXT_MODE_TETRODOMO_RECT = Scale.scaleRect(MENU_TEXT_MODE_TETRODOMO_RECT);
    MENU_TEXT_MODE_EASYSPIN_RECT =  Scale.scaleRect(MENU_TEXT_MODE_EASYSPIN_RECT);
    MENU_TEXT_MODE_NORMAL_RECT =    Scale.scaleRect(MENU_TEXT_MODE_NORMAL_RECT);
    MENU_TEXT_MODE_EXTREME_RECT =   Scale.scaleRect(MENU_TEXT_MODE_EXTREME_RECT);
    
    MENU_BUTTON_DIM = Scale.scaleDim(MENU_BUTTON_DIM);
    MENU_BUTTONS_POINT = Scale.scalePoint(MENU_BUTTONS_POINT);
    MENU_BUTTON_SEPARATION = Scale.scalePoint(MENU_BUTTON_SEPARATION);
    
    MENU_BUTTON_NEWGAME_RECT =  Scale.scaleRect(MENU_BUTTON_NEWGAME_RECT);
    MENU_BUTTON_LOADGAME_RECT = Scale.scaleRect(MENU_BUTTON_LOADGAME_RECT);
    MENU_BUTTON_SAVEGAME_RECT = Scale.scaleRect(MENU_BUTTON_SAVEGAME_RECT);
    MENU_BUTTON_STATS_RECT =    Scale.scaleRect(MENU_BUTTON_STATS_RECT);
    MENU_BUTTON_CREDITS_RECT =  Scale.scaleRect(MENU_BUTTON_CREDITS_RECT);
    MENU_BUTTON_EXIT_RECT =     Scale.scaleRect(MENU_BUTTON_EXIT_RECT);
    
    MENU_BUTTON_MODE_TETRODOMO_RECT = Scale.scaleRect(MENU_BUTTON_MODE_TETRODOMO_RECT);
    MENU_BUTTON_MODE_EASYSPIN_RECT =  Scale.scaleRect(MENU_BUTTON_MODE_EASYSPIN_RECT);
    MENU_BUTTON_MODE_NORMAL_RECT =    Scale.scaleRect(MENU_BUTTON_MODE_NORMAL_RECT);
    MENU_BUTTON_MODE_EXTREME_RECT =   Scale.scaleRect(MENU_BUTTON_MODE_EXTREME_RECT);
    
    MENU_IMG_RECT = Scale.scaleRect(MENU_IMG_RECT);
    MENU_IMG_LOGO_RECT = Scale.scaleRect(MENU_IMG_LOGO_RECT);
    
    STATS_IMG_LOGO_RECT = Scale.scaleRect(STATS_IMG_LOGO_RECT);
    STATS_IMG_STRIPE_RECT = Scale.scaleRect(STATS_IMG_STRIPE_RECT);
    STATS_IMG_SQUARE_RECT = Scale.scaleRect(STATS_IMG_SQUARE_RECT);
    STATS_IMG_SQUARE_SEPARATION = Scale.scalePoint(STATS_IMG_SQUARE_SEPARATION);
    STATS_IMG_RECTANGLE_RECT = Scale.scaleRect(STATS_IMG_RECTANGLE_RECT);
    STATS_IMG_RECTANGLE_SEPARATION_Y = Scale.scaleHeight(STATS_IMG_RECTANGLE_SEPARATION_Y);
    
    STATS_TEXT_LEVEL_RECT = Scale.scaleRect(STATS_TEXT_LEVEL_RECT);
    STATS_TEXT_POINTS_RECT = Scale.scaleRect(STATS_TEXT_POINTS_RECT);
    STATS_TEXT_ROWS_RECT = Scale.scaleRect(STATS_TEXT_ROWS_RECT);
    STATS_TEXT_DATE_RECT = Scale.scaleRect(STATS_TEXT_DATE_RECT);
    
    STATS_TEXT_LEVEL_VALUE_RECT = Scale.scaleRect(STATS_TEXT_LEVEL_VALUE_RECT);
    STATS_TEXT_POINTS_VALUE_RECT = Scale.scaleRect(STATS_TEXT_POINTS_VALUE_RECT);
    STATS_TEXT_ROWS_VALUE_RECT = Scale.scaleRect(STATS_TEXT_ROWS_VALUE_RECT);
    STATS_TEXT_DATE_VALUE_RECT = Scale.scaleRect(STATS_TEXT_DATE_VALUE_RECT);
    
    STATS_TEXT_VALUE_SEPARATION_Y = Scale.scaleHeight(STATS_TEXT_VALUE_SEPARATION_Y);
    
    STATS_TEXT_MODE_RECT = Scale.scaleRect(STATS_TEXT_MODE_RECT);
    
    
    BUTTON_DIM = Scale.scaleDim(BUTTON_DIM);
    BUTTON_DOWN_WIDTH = Scale.scaleWidth(BUTTON_DOWN_WIDTH);
    //BUTTON_SEPARATION_Y = Scale.scaleHeight(BUTTON_SEPARATION_Y);
    BUTTON_ROTATE_SEPARATION_Y = Scale.scaleHeight(BUTTON_ROTATE_SEPARATION_Y);
    //BOARD_BUTTONS_SEPARATION_Y = Scale.scaleHeight(BOARD_BUTTONS_SEPARATION_Y);

    BUTTON_IMAGE_LEFT_RECT = Scale.scaleRect(BUTTON_IMAGE_LEFT_RECT);
    BUTTON_IMAGE_RIGHT_RECT = Scale.scaleRect(BUTTON_IMAGE_RIGHT_RECT);
    BUTTON_IMAGE_DOWN_RECT = Scale.scaleRect(BUTTON_IMAGE_DOWN_RECT);
    BUTTON_IMAGE_ROTATE_RECT = Scale.scaleRect(BUTTON_IMAGE_ROTATE_RECT);
    
    BUTTON_LEFT_RECT = Scale.scaleRect(BUTTON_LEFT_RECT); 
    BUTTON_RIGHT_RECT = Scale.scaleRect(BUTTON_RIGHT_RECT); 
    BUTTON_DOWN_RECT = Scale.scaleRect(BUTTON_DOWN_RECT);
    BUTTON_ROTATE_RECT = Scale.scaleRect(BUTTON_ROTATE_RECT);
    
    BUTTON_IMAGE_SCALE_DIM = Scale.scaleDim(BUTTON_IMAGE_SCALE_DIM);
    BUTTON_IMAGE_ROTATE_SCALE_DIM = Scale.scaleDim(BUTTON_IMAGE_ROTATE_SCALE_DIM);
    //BUTTONS_SHAPE_DIM = Scale.scaleDim(BUTTONS_SHAPE_DIM);
    
    BOARD_CELL_DIM_SCALED = Scale.scaleDim(BOARD_CELL_DIM_SCALED);
    
    LOADING_IMG_LOGO_RECT = Scale.scaleRect(LOADING_IMG_LOGO_RECT);
    
    CONTROL_BUTTONS_POINT_Y = Scale.scaleHeight(CONTROL_BUTTONS_POINT_Y);
    
    OPTIONS_RECT = Scale.scaleRect(OPTIONS_RECT);
    OPTIONS_SEPARATION_Y = Scale.scaleHeight(OPTIONS_SEPARATION_Y);
    
    alreadyScaled = true;
    //otherConstants();
    
    println("screen dim(" + _SCREEN.width + ", " + _SCREEN.height + ")");
    println("screenwidth relative to 320x480 is dim(" + scaleWidth + ", " + scaleHeight + ")");
    println("translate to(" + translateX + ", " + translateY + ")");
    println("BUTTON_WIDTH: " + BUTTON_DIM.width);
    println("BUTTON_HEIGHT: " + BUTTON_DIM.height);
    println("BOARD_CELL_WIDTH: " + BOARD_CELL_DIM.width);
    println("BOARD_CELL_HEIGHT: " + BOARD_CELL_DIM.height);
    println("All was correctly scaled");
    return;
    
  }
  static void otherConstants(){
    
    LOADING_PIECES_RECT = Scale.scaleRect(LOADING_PIECES_RECT);
    
    BOARD_IMG_RECT = Scale.scaleRect(BOARD_IMG_RECT);
    BOARD_IMG_LOGO_RECT = Scale.scaleRect(BOARD_IMG_LOGO_RECT);
    
    BOARD_GRID_SEPARATION = Scale.scalePoint(BOARD_GRID_SEPARATION);
    BOARD_CELL_DIM = Scale.scaleDim(BOARD_CELL_DIM);
    BOARD_BORDER_LR = Scale.scaleWidth(BOARD_BORDER_LR);
    BOARD_BORDER_UB = Scale.scaleHeight(BOARD_BORDER_UB);
      
    BUFFER_PIECE_RECT = Scale.scaleRect(BUFFER_PIECE_RECT);
   
    
    alreadyScaled = true;
  }
}

