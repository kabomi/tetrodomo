// Tetrodomo
// Created by Imobach Martín 
// kabomi.es
class Menu{
  PImage menuImg;
  PImage logoImg;
  PImage[] statsImg;
  PImage[] backImg;
  PImage[] buttonImg;
  PImage[] squareImg;
  PImage ccImg;
  
  Button[] Buttons;
  Button[] MainButtons;
  Button[] ModeButtons;

  boolean isVisible = false;

  char pressedButton;
  boolean secondDepthLevel = false;
  char firstAction = _NO_BUTTON_PRESSED;
  
  char [] mainActions = new char[]{ 
    _NEW_GAME, 
    _LOAD_GAME,
    _SAVE_GAME,
    _STATS,
    //_OPTIONS,
    _CREDITS,
    _EXIT }; 
  String [] mainTexts = new String[]{
    MENU_TEXT_NEWGAME,
    MENU_TEXT_LOADGAME,
    MENU_TEXT_SAVEGAME,
    MENU_TEXT_STATS,
    //MENU_TEXT_OPTIONS,
    MENU_TEXT_CREDITS,
    MENU_TEXT_EXIT };
  Rect[] mainTextRects = new Rect[]{
    MENU_TEXT_NEWGAME_RECT,
    MENU_TEXT_LOADGAME_RECT,
    MENU_TEXT_SAVEGAME_RECT,
    MENU_TEXT_STATS_RECT,
    MENU_TEXT_CREDITS_RECT,
    MENU_TEXT_EXIT_RECT
  };
  Rect[] mainRects = new Rect[]{
    MENU_BUTTON_NEWGAME_RECT,
    MENU_BUTTON_LOADGAME_RECT,
    MENU_BUTTON_SAVEGAME_RECT,
    MENU_BUTTON_STATS_RECT,
    MENU_BUTTON_CREDITS_RECT,
    MENU_BUTTON_EXIT_RECT
  };
    
  char [] modeActions = new char[]{ 
    _GAME_MODE_TETRODOMO,
    _GAME_MODE_EASY_SPIN, 
    _GAME_MODE_NORMAL, 
    _GAME_MODE_EXTREME };
  String [] modeTexts = new String[]{
    MENU_TEXT_TETRODOMO,
    MENU_TEXT_EASY_SPIN,
    MENU_TEXT_NORMAL,
    MENU_TEXT_EXTREME };  
  Rect[] modeTextRects = new Rect[]{
    MENU_TEXT_MODE_TETRODOMO_RECT,
    MENU_TEXT_MODE_EASYSPIN_RECT,
    MENU_TEXT_MODE_NORMAL_RECT,
    MENU_TEXT_MODE_EXTREME_RECT
  };
  
  Rect[] modeRects = new Rect[]{
    MENU_BUTTON_MODE_TETRODOMO_RECT,
    MENU_BUTTON_MODE_EASYSPIN_RECT,
    MENU_BUTTON_MODE_NORMAL_RECT,
    MENU_BUTTON_MODE_EXTREME_RECT
  };
  String[] statTexts = new String[]{
    MENU_TEXT_STATS_LEVEL,
    MENU_TEXT_STATS_POINTS,
    MENU_TEXT_STATS_ROWS,
    MENU_TEXT_STATS_DATE
  };
  
  Rect[] statRects = new Rect[]{
    STATS_TEXT_LEVEL_RECT,
    STATS_TEXT_POINTS_RECT,
    STATS_TEXT_ROWS_RECT,
    STATS_TEXT_DATE_RECT
  };
    
  String[] texts;
  
  PFont menuExitFont =    createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_MENU_EXIT);
  PFont menuCreditsFont = createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_MENU_CREDITS);
  PFont menuStatsFont =   createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_MENU_STATS);
  PFont menuModeFont =    createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_MENU_MODE);
  PFont menuNormalFont=   createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_MENU_NORMAL_MODE);
  PFont statsModeFont =   createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_STATS_MODE);
  PFont statsTextFont =   createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_STATS_TEXT);
  PFont statsDateFont =   createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_STATS_TEXT_DATE);
  PFont licenseFont =     createFont(FONT_FILE_ARCHITECTSDAUGHTER, FONT_SIZE_CREDITS_TEXT_LICENSE);
  
  
  Stat[] loadedStats;
  
  String eventsBuffer = "";
  
  Timer linkTimer;
    
  Menu(){
    this.isVisible = false;
    pressedButton = _NO_BUTTON_PRESSED;
    
    this.loadImages();
    this.loadButtons();  
    loadStatsData();
    
    linkTimer = new Timer(_MENU_CREDITS_LINK_LENGTH_MS);
  }
  void loadImages(){
    this.menuImg = loadImage(FILE_IMG_MENU);
    this.logoImg = loadImage(FILE_IMG_LOGO_2);
    this.buttonImg = new PImage[2];
    this.buttonImg[0] = loadImage(FILE_IMG_MENU_BUTTON_SHAPE);
    this.buttonImg[1] = loadImage(FILE_IMG_MENU_BUTTON_SHAPE_2);
    this.squareImg = new PImage[MENU_SQUARE_TYPES.length + 1];
    for(int i= 0;i< MENU_SQUARE_TYPES.length;i++){
      this.squareImg[i] = loadImage(FILENAMEPREFIX_IMG_MENU_SQUARE + MENU_SQUARE_TYPES[i] + FILETYPE_PNG);
    }
    this.squareImg[MENU_SQUARE_TYPES.length] = loadImage(FILENAMEPREFIX_IMG_MENU_SQUARE + FILETYPE_PNG);
    this.statsImg = new PImage[4];
    this.statsImg[0] = loadImage(FILE_IMG_MENU_STATS_LOGO);
    this.statsImg[1] = loadImage(FILE_IMG_MENU_STATS_STRIPE);
    this.statsImg[2] = loadImage(FILE_IMG_MENU_STATS_SQUARE);
    this.statsImg[3] = loadImage(FILE_IMG_MENU_STATS_RECTANGLE);
    this.ccImg = loadImage(FILE_IMG_CC);
  }
  void loadButtons(){
    this.MainButtons = new Button[mainActions.length];
    for(int i = 0; i < mainActions.length; i++){
      this.MainButtons[i] = new FixedButton(this.buttonImg, mainRects[i], mainActions[i]);
    }
    this.ModeButtons = new Button[modeActions.length];
    for(int i = 0; i < modeActions.length; i++){ 
      this.ModeButtons[i] = new FixedButton(this.buttonImg, modeRects[i], modeActions[i]);
    }
  }
  
  void showButtons(){
    if(this.secondDepthLevel){
      this.Buttons = this.ModeButtons;
      this.texts = this.modeTexts;
    }else{
      this.Buttons = this.MainButtons;
      this.texts = this.mainTexts;
    }
  }
  
  void Draw(){
    flushEvents();
    boolean mustDrawStatsOrCredits = ((firstAction == _STATS || firstAction == _CREDITS) && secondDepthLevel);
    show();
    showButtons();
    
    try{
      if(mustDrawStatsOrCredits)
        drawStatsOrCredits();
      else
        drawMenu();
    }catch(Exception ex){
      println("Error drawing menu: " + ex.getMessage());
      clearEvents();
      inputEvent(_HIDE_MENU_SECOND_LEVEL);
      firstAction = _NO_BUTTON_PRESSED;
    }
  }
  void clearEvents(){
    eventsBuffer = "";
  }
  void flushEvents(){
    Logger.Log("Menu flushEvents:" + eventsBuffer);
    try{
      while(eventsBuffer.length() > 0){
        char event = eventsBuffer.charAt(0);
        eventsBuffer = eventsBuffer.substring(1);
        raiseEvent(event);
      }
    }catch(Exception ex){
      println("Input Buffer Exception: " + ex.getMessage() + "||| eventsBuffer: " + eventsBuffer);
      ex.printStackTrace(System.out);
      noLoop();
    }
  }
  void inputEvent(char test){
    if(eventsBuffer.length() < MAX_MENU_EVENTS_BUFFER){
      eventsBuffer = eventsBuffer + str(test);
    }
  }
  void raiseEvent(char eventType){
    if(eventType == _SHOW_MENU_SECOND_LEVEL)
      Menu.secondDepthLevel = true;
    if(eventType == _HIDE_MENU_SECOND_LEVEL){
        Menu.secondDepthLevel = false;
    }
  }
  void drawStatsOrCredits(){
    background(BACKGROUND_GRAY);
    drawImage(this.statsImg[1], STATS_IMG_STRIPE_RECT);
    drawImage(this.statsImg[0], STATS_IMG_LOGO_RECT);

    if(firstAction == _CREDITS){
      fill(_BLACK);
      drawText(statsTextFont, "Game programming by Imobach Martin", Scale.scaleRect(Rect.Rect(20,70,400,200)));
      fill(26,126,176);
      drawText(statsTextFont, "kabomi.es", Scale.scaleRect(Rect.Rect(20,85,400,200)));
      fill(_BLACK);
      drawText(statsTextFont, "Graphics design by Daniel Placeres Santana", Scale.scaleRect(Rect.Rect(20,105,400,200)));
      fill(26,126,176);
      drawText(statsTextFont, "dani.ps90@gmail.com", Scale.scaleRect(Rect.Rect(20,120,400,200)));
      fill(_BLACK);
      drawText(statsTextFont, "Special thanks to Caro & probotx", Scale.scaleRect(Rect.Rect(20,140,400,200)));
      drawText(menuCreditsFont, "Music Theme 'DisTOrATion' by TDeeJay", Scale.scaleRect(Rect.Rect(20,170,400,400)));
      drawText(menuCreditsFont, getCredits(), Scale.scaleRect(Rect.Rect(20,190,400,400)));
      drawText(licenseFont,   "Music and effects are licensed under\nthe Attribution License.", Scale.scaleRect(Rect.Rect(20,455,400,100)));
      drawImage(ccImg, Scale.scaleRect(Rect.Rect(190,455,20,20)));
    }else{
    for(int i=0; i < modeActions.length; i++){
      drawImage(this.statsImg[2], STATS_IMG_SQUARE_RECT.copyAndAdd(0,STATS_IMG_SQUARE_SEPARATION.y*i));
      drawImage(this.statsImg[2], STATS_IMG_SQUARE_RECT.copyAndAdd(STATS_IMG_SQUARE_SEPARATION.x,STATS_IMG_SQUARE_SEPARATION.y*i));
      drawImage(this.statsImg[3], STATS_IMG_RECTANGLE_RECT.copyAndAdd(0,STATS_IMG_RECTANGLE_SEPARATION_Y*i));
      fill(_WHITE);
      textAlign(LEFT);
      drawText(menuStatsFont, statTexts[i], statRects[i]);
      fill(_BLACK);
      textAlign(CENTER);
      drawText(statsModeFont, modeTexts[i], STATS_TEXT_MODE_RECT.copyAndAdd(0,STATS_IMG_SQUARE_SEPARATION.y*i));
      drawText(statsTextFont, str(loadedStats[i].level)    , STATS_TEXT_LEVEL_VALUE_RECT.copyAndAdd(0, STATS_TEXT_VALUE_SEPARATION_Y*i));
      drawText(statsTextFont, str(loadedStats[i].points)   , STATS_TEXT_POINTS_VALUE_RECT.copyAndAdd(0, STATS_TEXT_VALUE_SEPARATION_Y*i));
      drawText(statsTextFont, str(loadedStats[i].totalRows), STATS_TEXT_ROWS_VALUE_RECT.copyAndAdd(0, STATS_TEXT_VALUE_SEPARATION_Y*i));
      drawText(statsDateFont, loadedStats[i].date          , STATS_TEXT_DATE_VALUE_RECT.copyAndAdd(0, STATS_TEXT_VALUE_SEPARATION_Y*i));
    }
    }
  }
  String getCredits(){
    String lines = "";
    lines += "ccmixter.org/files/TDeeJay/20541\n";
    lines += "Music Theme 'Sooner or Later' by The_Extreme\n";
    lines += "ccmixter.org/files/The_Extreme/16019\n";
    lines += "Effects\n";
    lines += "'Button' by Sergenious\n";
    lines += "freesound.org/people/Sergenious/sounds/55845/\n";
    //lines += "\n";
    lines += "'Full Lines' & 'Pause' & 'Piece placed' by fins\n";
    lines += "freesound.org/people/fins/sounds/191754/\n";
    lines += "freesound.org/people/fins/sounds/146725/\n";
    lines += "freesound.org/people/fins/sounds/146718/\n";
    //lines += "\n";
    lines += "'Level Up' by LloydEvans09\n";
    lines += "freesound.org/people/LloydEvans09/sounds/187024/\n";
    //lines += "\n";
    lines += "'GameOver' by Corsica_S\n";
    lines += "facebook.com/corsica.ess\n";
    lines += "freesound.org/people/Corsica_S/sounds/72866/\n";
    return lines;
  }
  void loadStatsData(){
    Stat[] stats = new Stat[modeActions.length];
    JSONorg.JSONObject json = null;
    JSONorg.JSONArray loadedStats = null;
    for(int i =0; i < modeActions.length;i++){
      try{
        json = new JSONorg.JSONObject(Utils.ReadAll(dataPath(FILE_JSON_STATS + modeActions[i] + FILETYPE_JSON)));
        loadedStats = json.getJSONArray("STATS");
        int maxPointsIndex = 0;
        int maxPoints = 0;
        Stat stat = new Stat();
        for(int j=0; j < loadedStats.length();j++){
          stat = (Stat)Util.jsonToObj(loadedStats.getJSONObject(j), stat);
          if(stat.points > maxPoints){
            maxPointsIndex = j;
            maxPoints = stat.points;
          }
        }
        stats[i] = (Stat)Util.jsonToObj(loadedStats.getJSONObject(maxPointsIndex), stat);
        stats[i].level = stats[i].level + 1;
        stats[i].date = stats[i].date.substring(2,10);
      }catch(Exception ex){
        Logger.Log("Could not load stats for " + modeTexts[i]);
        Logger.Log("Error: " + ex.getMessage());
        stats[i] = new Stat();
        stats[i].date = stats[i].date.substring(2,10);
      }
    }
    this.loadedStats = stats;
  }
  void drawMenu(){
    background(BACKGROUND_R, BACKGROUND_G, BACKGROUND_B);
    drawImage(this.menuImg, MENU_IMG_RECT);
    drawImage(this.logoImg, MENU_IMG_LOGO_RECT);
    
    fill(secondDepthLevel?_WHITE : _BLACK);
    stroke(0);
    
    boolean isButtonPressed = (pressedButton != _NO_BUTTON_PRESSED);
    int buttonCount = 0;
    for(int i=0; i< MENU_BUTTONS_IMG.length; i++){
      char type = (secondDepthLevel?MENU_MODE_BUTTONS_IMG[i]:MENU_BUTTONS_IMG[i]);
      if(type == _BUTTON_CHAR){
        if(buttonCount >= this.Buttons.length)
          continue;
        tint(_WHITE, _SEMI_TRANSPARENT);
        if(isButtonPressed && this.Buttons[buttonCount].type == pressedButton){
          drawImage(this.buttonImg[1], this.Buttons[buttonCount].rect); 
        }else{
          drawImage(this.Buttons[buttonCount].stateImg[0], this.Buttons[buttonCount].rect);
        }
        noTint();  
        buttonCount++;
      }else{
        int index = findSquareByValue(type);
        int column = i%3;
        int row = floor(i/3)%5;
        image(this.squareImg[index], MENU_BUTTONS_POINT.x + MENU_BUTTON_SEPARATION.x*column, MENU_BUTTONS_POINT.y  + MENU_BUTTON_SEPARATION.y*row, MENU_BUTTON_DIM.width, MENU_BUTTON_DIM.height);
      }
    }
    drawMenuTexts();
  }
  int findSquareByValue(char type){
    for(int i=0; i< MENU_SQUARE_TYPES.length;i++){
      if(type == MENU_SQUARE_TYPES[i])
        return i;
    }
    return 0;
  }
  void drawMenuTexts(){
    fill(_BLACK);
    textAlign(CENTER);
    for(int i = 0; i < this.Buttons.length; i++){
      textFont(mainFont);
      if(!secondDepthLevel){
        switch(this.Buttons[i].type){
          case _STATS:
            textFont(menuStatsFont);
            break;
          case _CREDITS:
            textFont(menuCreditsFont);
            break;
          case _EXIT:
            textFont(menuExitFont);
            break;
        }
      }else{
        
        switch(this.Buttons[i].type){
          case _GAME_MODE_NORMAL:
            textFont(menuNormalFont);
            break;
          default:
            textFont(menuModeFont);
            break;
        }
      }
      Rect newRect = this.Buttons[i].rect.copy();
      newRect.moveX(secondDepthLevel?modeTextRects[i].x:mainTextRects[i].x);
      newRect.moveY(secondDepthLevel?modeTextRects[i].y:mainTextRects[i].y);
      newRect.setDim(MENU_TEXT_BUTTON_DIM);
      drawText(texts[i], newRect);
    }
    textAlign(LEFT);
  }
  
  char getButtonFromPoint(Point newPoint){
    return getButtonFromPoint(newPoint.x, newPoint.y);
  }
  char getButtonFromPoint(int posx, int posy){
    //THIS METHOD IS USED IN ANDROID (TOUCH EVENTS)
    for(int i = 0; i < this.Buttons.length; i++){
      if(this.Buttons[i].isPointWithinBoundaries(new Point(posx, posy))){
        return this.Buttons[i].type;
      }
    }
    return _NO_BUTTON_PRESSED;
  }
  
  Button getButton(char test){
    for(int i = 0; i < this.Buttons.length; i++){
      if(this.Buttons[i].type == test){
        return this.Buttons[i];
      }
    }
    return null;
  }
  Button getPressedButton(){
    return getButton(pressedButton);
  }
  
  void setPressedButton(char test){
    pressedButton = test;
  }
  void releaseButton(){
    pressedButton = _NO_BUTTON_PRESSED;
  }
  void show(){
    this.isVisible = true;
  }
  void hide(){
    this.isVisible = false;
    clearEvents();
    inputEvent(_HIDE_MENU_SECOND_LEVEL);
  }
  void onMenuPressed(Point point){
    if(Menu.isVisible){
      Logger.Log("MENU________________________");
      char menuButton = Menu.getButtonFromPoint(point);
      Menu.setPressedButton(menuButton);
      Logger.Log("Pressed:" + menuButton);
      if(!Menu.secondDepthLevel){
        firstAction = menuButton;
      }
    }
  }
  void onMenuReleased(Point point){
    if(!Menu.isVisible && initialized && Game.gameOver){
      Restart();
      return;
    }
    if(Menu.isVisible){
      char menuButton = Menu.getButtonFromPoint(point.x, point.y);
      Menu.releaseButton();
      Logger.Log("MouseReleased:" + menuButton);
      if(!Menu.secondDepthLevel){
        switch(menuButton){
          case _NEW_GAME:
          case _LOAD_GAME:
          case _STATS:
          case _CREDITS:
            Effects.playMediaFile(FILE_EFFECT_BUTTON);
            inputEvent(_SHOW_MENU_SECOND_LEVEL); 
            break;
          case _SAVE_GAME:
            Effects.playMediaFile(FILE_EFFECT_BUTTON);
            SaveGame();
            break;
          case _OPTIONS:
            break;
          case _EXIT:
            Effects.playMediaFile(FILE_EFFECT_BUTTON);
            Exit();
        };
        
      }else{
        switch(firstAction){
          case _NEW_GAME:
            Effects.playMediaFile(FILE_EFFECT_BUTTON);
            inputEvent(_HIDE_MENU_SECOND_LEVEL);
            SetGameMode(menuButton);
            Restart();
            break;
          case _LOAD_GAME:
            Effects.playMediaFile(FILE_EFFECT_BUTTON);
            SetGameMode(menuButton);
            LoadGame();
            break;
          case _CREDITS:
            if(linkTimer.isFinished()){
              linkTimer = new Timer(_MENU_CREDITS_LINK_LENGTH_MS);
              linkTimer.start();
              link(AUTHOR_LINK);
            }
            break;
        }
      }
    }
  }
  void onMenuMoved(Point point){
    if(Menu.isVisible){
      char menuButton = Menu.getButtonFromPoint(point.x, point.y);
      Menu.setPressedButton(menuButton);
      Logger.Log("MouseMoved:" + menuButton);
      if(!Menu.secondDepthLevel){
        firstAction = menuButton;
      }
    }
  }
}

