static class PointMatrix{
  Point point;
  
  int numColumns;
  int numRows;
  
  char [][]Matrix = null;
  
  static PointMatrix PointMatrix(char [][] fromMatrix, Point startPoint){
    return new PointMatrix(fromMatrix, startPoint);
  }
  static PointMatrix PointMatrix(boolean [][] fromMatrix, Point startPoint){
    return new PointMatrix(fromMatrix, startPoint);
  }
  static PointMatrix PointMatrixOnSet(char [][] fromMatrix){
    return new PointMatrix(fromMatrix, Point.PointOnSet());
  }
  static PointMatrix PointMatrixOnSet(boolean [][] fromMatrix){
    return new PointMatrix(fromMatrix, Point.PointOnSet());
  }
  PointMatrix(char [][] fromMatrix, Point startPoint){
    assert fromMatrix.length > 0 && fromMatrix[0].length > 0;
    
    Matrix = fromMatrix;
    numColumns = Matrix.length;
    numRows = Matrix[0].length;
    point = new Point(startPoint.x, startPoint.y);
  }
  PointMatrix(boolean [][] fromMatrix, Point startPoint){
    assert fromMatrix.length > 0 && fromMatrix[0].length > 0;
    
    Matrix = toCharMatrix(fromMatrix);
    numColumns = Matrix.length;
    numRows = Matrix[0].length;
    point = new Point(startPoint.x, startPoint.y);
  }
  char[][] toCharMatrix(boolean[][] fromMatrix){
    char[][] charMatrix = new char[fromMatrix.length][fromMatrix[0].length];
    for(int j= 0 ; j < fromMatrix[0].length;j++){
      for(int i=0 ; i < fromMatrix.length; i++){
        charMatrix[i][j] = (fromMatrix[i][j]?_TRUE_CHAR:_FALSE_CHAR);
      }
    }
    return charMatrix;
  }
  
  void moveTo(Point point){
    this.point = new Point(point.x, point.y);
  }
  void moveLeft(int units){
    assert units >= 0;
    point.moveX(-units);
  }
  void moveRight(int units){
    assert units >= 0;
    point.moveX(units);
  }
  void moveDown(int units){
    assert units >= 0;
    point.moveY(units);
  }
  void moveUp(int units){
    assert units >= 0;
    point.moveY(-units);
  }
  void moveRightDown(int units){
    assert units >= 0;
    point.moveBoth(units);
  }
  char getValue(int x, int y){
    return Matrix[x][y];
  }
  boolean getBool(int x, int y){
    return (Matrix[x][y] == _TRUE_CHAR);
  }
  void setValue(int x, int y, char value){
    Matrix[x][y] = value;
  }
  int lastColumn(){
    return numColumns - 1;
  }
  int lastRow(){
    return numRows - 1;
  }
  void Print(){
    String line = "";
    for(int j= 0 ; j < Matrix[0].length;j++){
      for(int i=0 ; i < Matrix.length; i++){
        char value = ((Matrix[i][j] == _TRUE_CHAR)?'1':((Matrix[i][j] == _FALSE_CHAR)?'0':Matrix[i][j]));
        line += value;
      }
      println(line);
      line = "";
    }
  }
}

