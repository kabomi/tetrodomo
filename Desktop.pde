//****************************
//**********EVENTS***********
//****************************
/*
void keyPressed(){
  switch(key){
    case CODED:
        if(!Menu.isVisible && initialized){
          //La pieza queda inmovil
          if(Game.isCurrentPiecePlaced())
            return;
          if (keyCode == UP) {
            Game.inputEvent(_ROTATE);
          }
          if (keyCode == DOWN) {
            Game.inputEvent(_DOWN);
          } 
          if (keyCode == RIGHT){
            Game.inputEvent(_RIGHT);
          }
          if (keyCode == LEFT){
            Game.inputEvent(_LEFT);
          }
        }
      return; 
    case _NEW_GAME:
      Restart();
      return;
    case _SAVE_GAME:
      SaveGame();
      loop();
      return;
    case _LOAD_GAME:
      LoadGame();
      loop();
      return;
  }
  if(initialized){
    switch(key){
      case '+':
        Game.inputEvent(_LEVEL_UP);
        break;
      case '-':
        Game.inputEvent(_LEVEL_DOWN);
        break;
      case 'o':
        Game.inputEvent(_CONTROLS_VIEW);
        break;
      case 'm':
        Game.inputEvent(_MUSIC);
        break;
      case ' ':
        Game.inputEvent(_PIECE_DOWN);
        break;  
      case TAB:
        Game.inputEvent(_PIECE_RESET);
        break;
      case 't':
        Game.Settings.prevShape();
        Logger.Log("ShapeType:" + Game.Settings.shapeType);
        break;
      case 'r':
        Game.Settings.nextShape();
        Logger.Log("ShapeType:" + Game.Settings.shapeType);
        break;
      case 'g':
        Game.Settings.ghost = !Game.Settings.ghost;
        Logger.Log("Ghost " + (Game.Settings.ghost?"Activado":"Desactivado"));
        break;
      case 'p':
        onBackPressed();
        break; 
    }
  }else{
    switch(key){
      case 'p':
        onBackPressed();
        break; 
    }
  }
}
void onBackPressed() {
  println("onBackPressed");
  if(Menu.isVisible){
    if(Menu.secondDepthLevel){
      Menu.inputEvent(_HIDE_MENU_SECOND_LEVEL);
    }else{
      if(initialized){
        Resume();
      }
    }
  }else{
    Pause();
  }
}
void mousePressed(){
  Menu.onMenuPressed(Point.Point(mouseX - Scale.translateX, mouseY - Scale.translateY));
}
void mouseReleased(){
  Menu.onMenuReleased(Point.Point(mouseX - Scale.translateX, mouseY - Scale.translateY));
}
void mouseMoved(){
  Menu.onMenuMoved(Point.Point(mouseX - Scale.translateX, mouseY - Scale.translateY));
}
void setPortraitOrientation(){
}
*/
