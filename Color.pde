//cambiar a algo tipo struct + clase static
class Color{
  char type;
  int r;
  int g;
  int b;
  float a;
  void Init(int r, int g, int b, float a){
    this.r = r; this.g= g; this.b = b; this.a = a;
  }
  Color(char t, float a){
    switch(t){
      case _SHAPE_RECT://e1c819//Cuadrado
        //Init(255,225,25,a);
        Init(252,207,33,a);//R252G207B33.A
        break;
      case _SHAPE_S://37ff19//S
        //Init(55,255,25,a);
        Init(140,198,63,a);//R140G198B63.B
        break;
      case _SHAPE_Z://ff1919//Z
        //Init(255,25,25,a);
        Init(193,39,45,a);//R193G39B45.C
        break;
      case _SHAPE_BAR://37e1ff//Barra
        //Init(25,255,225,a);
        Init(158,169,245,a);//R158G169B245.D
        break;
      case _SHAPE_TRIANGLE://ff19ff//Triangulo
        //Init(255,25,255,a);
        Init(141,80,143,a);//R141G80B143.E
        break;
      case _SHAPE_P://374bff//P
        //Init(25,55,255,a);
        Init(50,99,139,a);//R50G99B139.F
        break;
      case _SHAPE_L://ff4b00//L
        //Init(255,125,25,a);
        Init(247,147,30,a);//R247G147B30.G
        break;
      case _SHAPE_TROMINO_BOOMERANG://fefefe//¬
        //Init(239,239,239,a);
        Init(157,157,157,a);//R157G157B157.H
        break;
      case _SHAPE_TROMINO_I://501313//I
        //Init(80,19,19,a);
        Init(115,99,87,a);//R115G99B87.I
        break;
      case _SHAPE_DOMINO://ff4b00//¡
        //Init(0,0,0,a);
        Init(77,77,77,a);//R77G77B77.J
        break;
      case _SHAPE_MONOMINO://aaaaaa//#
        //Init(170,170,170,a);
        Init(15,136,76,a);//R15G136B76.K
        break;
      default:
        Init(255,255,255,a);
        break;
    }
  }
}
