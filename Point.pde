static class Point{
  int x;
  int y;
  
  static Point Point(int px, int py){
    return new Point(px,py);
  }
  static Point PointOnSet(){
    return new Point(0,0);
  }
  
  Point(int px, int py){
    x = px;
    y = py;
  }
  
  void moveX(int units){
    x += units;
  }
  void moveY(int units){
    y += units;
  }
  void moveBoth(int units){
    moveX(units);
    moveY(units);
  }
  boolean equals(Point point){
    return (this.x == point.x) && (this.y == point.y);
  }

}
