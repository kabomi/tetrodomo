static class Rect{
  int width;
  int height;
  int x;
  int y;
  
  Point point;
  Dim dim;
  
  static Rect Rect(int x, int y, Dim dim){
    return new Rect(Point.Point(x,y), dim);
  }
  static Rect Rect(int x, int y, int width, int height){
    return new Rect(Point.Point(x, y), Dim.Dim(width, height));
  }
  static Rect Rect(Point point, Dim dim){
    return new Rect(point, dim);
  }
  static Rect RectOnSet(int width, int height){
    return new Rect(Point.PointOnSet(), width, height);
  }

  Rect(Point point, Dim dim){
    setDim(dim);
    moveTo(point);
  }
  Rect(Point point, int width, int height){
    this.width = width;
    this.height = height;
    moveTo(point);
  }
  void moveTo(Point point){
    this.point = new Point(point.x, point.y);
    this.x = point.x;
    this.y = point.y;
  }
  void moveX(int units){
    this.point.x += units;
    this.x += units;
  }
  void moveY(int units){
    this.point.y += units;
    this.y += units;
  }
  void setDim(Dim dim){
    this.dim = new Dim(dim.width, dim.height);
    this.width = dim.width;
    this.height = dim.height;
  }
  Rect copy(){
    return Rect.Rect(this.point, this.dim);
  }
  Rect copyAndAdd(Point addValue){
    return Rect.Rect(Point.Point(this.point.x + addValue.x, this.point.y + addValue.y) , this.dim);
  }
  Rect copyAndAdd(int addValueX, int addValueY){
    return Rect.Rect(Point.Point(this.point.x + addValueX, this.point.y + addValueY) , this.dim);
  }
}
