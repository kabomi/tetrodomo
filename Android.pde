
boolean actionDown = false;
boolean touchReleased = true;
Point lastMotion = null;
Timer moveTimer = new Timer();
Timer tapTimer = new Timer();
Timer doubleTapTimer = new Timer();
Timer longPressTimer = new Timer();
Timer pieceDownTimer = new Timer();
Point lastTap = null;
Point newPoint = null;
char pressedButton = _NO_BUTTON_PRESSED;

public boolean surfaceTouchEvent(MotionEvent event) {
  if(splashTimer == null || !splashTimer.isFinished())
    return super.surfaceTouchEvent(event);
  int eventAction = event.getAction();
  newPoint = Point.Point((int)event.getX(), (int)event.getY());
  newPoint.moveX(- Scale.translateX);
  if(initialized && !Menu.isVisible)
    newPoint.moveY(- Scale.translateY*2);
  else
    newPoint.moveY(- Scale.translateY);
  Logger.Log("Log touch event: " + eventAction);
  pressedButton = getButtonFromPoint();
  
  if(Menu.isVisible){
    menuTouchEvent(eventAction);
    return super.surfaceTouchEvent(event);
  }
  if(!Game.isCurrentPiecePlaced())
    switch(eventAction){
      case _ACTION_DOWN:
        Logger.Log("PressDown (" + newPoint.x + ", " + newPoint.y + ")");
        onPressDown();
        break;
      case _ACTION_MOVE:
        if(isMoving()){
          tryMove();
        }else{
          if(isLongPressing()){
            doLongPress();
          }
        }
        break;    
      case _ACTION_UP:
        touchReleased = true;
        if(!tapTimer.isFinished()){
          if(isDoubleTapping()){
            doDoubleTap();
          }else{
            if(tapDistance()){
              setDoubleTapStart();
              doTap();
              Logger.Log("PressUp (" + newPoint.x + ", " + newPoint.y + ")");
            }else{
              onMoveTo(lastTap);
            }
          }
        }
        actionDown = false;
        break;
    }
    
  if(eventAction == _ACTION_UP && Game.gameOver)
    Restart();
    
  return super.surfaceTouchEvent(event);
}
char getButtonFromPoint(){
  char button = _NO_BUTTON_PRESSED;
  if(Menu.isVisible){
    button = Menu.getButtonFromPoint(newPoint);
    Logger.Log("Se ha pulsado sobre:" + button);
  }else{
    button = Game.input.getButtonFromPoint(newPoint);
    Logger.Log("Se ha pulsado sobre:" + button + "(" + newPoint.x + ", " + newPoint.y + ")");
  }
  return button;
}
void menuTouchEvent(int eventAction){
  switch(eventAction){
    case _ACTION_DOWN:
      Logger.Log("Menu PressDown (" + newPoint.x + ", " + newPoint.y + ")");
      Menu.onMenuPressed(newPoint);
      break;
    case _ACTION_MOVE:
      Logger.Log("Menu Move (" + newPoint.x + ", " + newPoint.y + ")");
      Menu.onMenuMoved(newPoint);
      break;    
    case _ACTION_UP:
      Logger.Log("Menu Release (" + newPoint.x + ", " + newPoint.y + ")");
      Menu.onMenuReleased(newPoint);
      break;
  }
}
void onPressDown(){
  if(pressedButton != _NO_BUTTON_PRESSED && 
     pressedButton != _MUSIC && 
     pressedButton != _CONTROLS_VIEW){
    Game.inputEvent(pressedButton);
  }else{
    setMotionStart();
  }
  setPressStart();
}
void setMotionStart(){
  actionDown = true;
  lastMotion = newPoint;
  moveTimer = new Timer(_GESTURE_MOVE_LENGTH_MS);
  moveTimer.start();
}
void setPressStart(){
  tapTimer = new Timer(_GESTURE_TAP_LENGTH_MS);
  tapTimer.start();
  lastTap = newPoint;
  longPressTimer = new Timer(_GESTURE_LONGPRESS_LENGTH_MS);
  longPressTimer.start();
}
boolean isMoving(){
  return actionDown && moveTimer.isFinished();
}
void tryMove(){
  if(!lastMotion.equals(newPoint)){
    if(onMoveTo(lastMotion))
      lastMotion = newPoint;
  }
}
boolean onMoveTo(Point lastMotion)
{
  if(pressedButton != _NO_BUTTON_PRESSED)
    return false;
  int diffX = abs(newPoint.x - lastMotion.x);
  int diffY = abs(newPoint.y - lastMotion.y);
  boolean xAxis = (diffX > diffY);
  
  Logger.Log("MOVE (" + newPoint.x + ", " + newPoint.y + ")(" + lastMotion.x + ", " + lastMotion.y + ")");
  if(xAxis){
    if(newPoint.x < lastMotion.x)
      return gestureSwing(_LEFT, diffX);
    if(newPoint.x > lastMotion.x)
      return gestureSwing(_RIGHT, diffX);
  }else{
    if(newPoint.y < lastMotion.y)
      Logger.Log("UP");
    if(newPoint.y > lastMotion.y)
      return gestureSwing(_DOWN, diffY);
  }
  return true;
}
boolean gestureSwing(char direction, int distance){
  int cellDimension = BOARD_CELL_DIM_SCALED.width;
  int fullSwingReactivity = Integer.MAX_VALUE;
  if(direction == _DOWN){
    cellDimension = BOARD_CELL_DIM_SCALED.height;
    fullSwingReactivity = SWING_DOWN_TILL_PIECE_DOWN_REACTIVITY;
  }
  int moveTimes = floor(distance/cellDimension);
  if(moveTimes == 0)
    return false;
  if(moveTimes < fullSwingReactivity){
    Logger.Log("direction: " + direction + "  " + moveTimes + " moveTimes");
    for(int i = 0; i < moveTimes;i++)
      Game.inputEvent(direction);
  }else{
    if(touchReleased && pieceDownTimer.isFinished()){
      pieceDownTimer = new Timer(_GESTURE_FULLSWING_DOWN_LENGTH_MS);
      pieceDownTimer.start();
      touchReleased = false;
      Logger.Log("PIECE_DOWN");
      Game.inputEvent(_PIECE_DOWN);
    }
  }
  return true;
}
boolean isLongPressing(){
  return !longPressTimer.isPaused && longPressTimer.isFinished();
}
void doLongPress(){
  Logger.Log("LONGPRESS (" + newPoint.x + ", " + newPoint.y + ")");
  if( pressedButton == _DOWN)
    Game.inputEvent(_PIECE_DOWN);
  longPressTimer.Pause();
}
boolean isDoubleTapping(){
  return !doubleTapTimer.isPaused && !doubleTapTimer.isFinished();
}
void doDoubleTap(){
  Logger.Log("DOUBLETAP (" + newPoint.x + ", " + newPoint.y + ")");
  if( pressedButton == _NO_BUTTON_PRESSED)
    Game.inputEvent(_ROTATE);
  else{
    if( pressedButton == _DOWN)
      Game.inputEvent(_PIECE_DOWN);
  }
}
boolean tapDistance(){
  int reactivityX = floor(0.1*lastTap.x);
  int reactivityY = floor(0.1*lastTap.y);
  float diffX = abs(newPoint.x - lastTap.x);
  float diffY = abs(newPoint.y - lastTap.y);
  return (diffX < reactivityX) &&
         (diffY < reactivityY);
}
void setDoubleTapStart(){
  doubleTapTimer = new Timer(_GESTURE_DOUBLETAP_LENGTH_MS);
  doubleTapTimer.start();
}
void doTap(){
  Logger.Log("MYTAP (" + newPoint.x + ", " + newPoint.y + ")");
  if( pressedButton == _NO_BUTTON_PRESSED)
    Game.inputEvent(_ROTATE);
  if(pressedButton == _MUSIC || pressedButton == _CONTROLS_VIEW)
    Game.inputEvent(pressedButton);
}

void onPause(){
  Pause();
  super.onPause();
}

void onResume(){
  Resume();
  super.onResume();
}

void onBackPressed() {
  Logger.Log("onBackPressed");
  Logger.Log("initialized:" + initialized);
  Logger.Log("Menu.isVisible:" + Menu.isVisible);
  Logger.Log("Menu.secondDepthLevel:" + Menu.secondDepthLevel);
  Logger.Log("gameMode:" + GameMode);
  if(Menu.isVisible){
    if(Menu.secondDepthLevel){
      Menu.inputEvent(_HIDE_MENU_SECOND_LEVEL);
    }else{
      if(initialized){
        Resume();
      }
    }
  }else{
    Pause();
  }
}
void onMenuPressed() {
  Logger.Log("onMenuPressed");
  if(Menu.isVisible){
    if(initialized){
      Logger.Log("Launch menu options");
    }
  }else{
    Pause();
  }
}

void keyPressed(){
  if(key == CODED){
    if (keyCode == BACK) {
      Logger.Log("Prevent back button to quit by default");
      keyCode = 0;  // don't quit by default
    } 
    if (keyCode == MENU){
      onMenuPressed();
      keyCode = 0;  // MENU provokes not to fire surfaceTouchEvent next time
    }
    return;  
  }
}

void setPortraitOrientation(){
  orientation(PORTRAIT);
}

