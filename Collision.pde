// Tetrodomo
// Created by Imobach Martín 
// kabomi.es
static class CollisionMatrix{
  int numRows;
  int numColumns;
  public char [][]Matrix = null;
  boolean hasBorder = true;
  
  static CollisionMatrix CollisionMatrixWithBorder(){
    return new CollisionMatrix(true);
  }
  private CollisionMatrix(boolean hasBorder){
    this.hasBorder = hasBorder;
    this.initialize();
  }
  CollisionMatrix(){
    this.initialize();
  }
  void initialize(){
    this.numRows = ROWS_NUM + (hasBorder?_ROWS_PAD:0);
    this.numColumns = COLUMNS_NUM + (hasBorder?_COLUMNS_PAD:0);
    this.Matrix = new char[numColumns][numRows];
    this.resetMatrix();
  }
  void resetMatrix(){
    assert numColumns > 0 && numRows > 0;
    assert Matrix != null;
    for(int i = 0;i < this.numColumns;i++){
      for(int j= 0; j < this.numRows; j++){
        this.Matrix[i][j] = (isBorderCell(i,j)?_TRUE_CHAR:_FALSE_CHAR);
      }
    }
  }
  boolean isBorderCell(int column, int row){
    if(!hasBorder){
      return false;
    }else{
      return (column == 0 || column == (this.numColumns -1) || row == 0 || row == (this.numRows -1));
    }
  }
  
  boolean isBorderedMatrixWithinBoundaries(PointMatrix testMatrix){
    //Logger.active = true;
    if(this.hasBorder)
      testMatrix.moveRightDown(1);
    boolean result = isMatrixWithinBoundaries(testMatrix);
    //Logger.active = false;
    return result;
  }
  boolean isMatrixLastCellWithinRightBorder(PointMatrix testMatrix){
    int lastMatrixCollidingColumn = 0;
    for(int i = 0; i < testMatrix.numColumns;i++){
      for(int j= 0; j < testMatrix.numRows; j++){
        if((testMatrix.getValue(i,j) == _COLLISION_CHAR) && 
           (i > lastMatrixCollidingColumn)){
          lastMatrixCollidingColumn++;
        }
      }
    }
    if((testMatrix.point.x + lastMatrixCollidingColumn) >= (this.numColumns - 1)){
      return _COLLISION;
    }
    return _NO_COLLISION;
  }
  boolean isMatrixFirstCellWithinLeftBorder(PointMatrix testMatrix){
    int firstMatrixCollidingColumn = testMatrix.numColumns - 1;
    for(int i = testMatrix.numColumns - 1; i >= 0;i--){
      for(int j= 0; j < testMatrix.numRows; j++){
        if((testMatrix.getValue(i,j) == _COLLISION_CHAR) && 
           (i < firstMatrixCollidingColumn)){
          firstMatrixCollidingColumn--;
        }
      }
    }
    if((testMatrix.point.x + firstMatrixCollidingColumn) <= 0){
      return _COLLISION;
    }
    return _NO_COLLISION;
  }
  boolean isMatrixFirstCellWithinDownBorder(PointMatrix testMatrix){
    int firstMatrixCollidingRow = testMatrix.numRows - 1;
    for(int i = 0; i < testMatrix.numColumns;i++){
      for(int j= testMatrix.numRows - 1; j >= 0; j--){
        if((testMatrix.getValue(i,j) == _COLLISION_CHAR) && 
           (i < firstMatrixCollidingRow)){
          firstMatrixCollidingRow--;
        }
      }
    }
    if((testMatrix.point.y + firstMatrixCollidingRow) <= 0){
      return _COLLISION;
    }
    return _NO_COLLISION;
  }
  boolean isMatrixWithinBoundaries(PointMatrix testMatrix){
    assert Matrix != null;
    assert testMatrix.Matrix.length > 0 && testMatrix.Matrix[0].length > 0;
    
    if(!isMatrixLastCellWithinRightBorder(testMatrix)){
      return _COLLISION;
    }
    if(!isMatrixFirstCellWithinLeftBorder(testMatrix)){
      return _COLLISION;
    }
    
    testMatrix = adjustMatrixAccordingToPosition(testMatrix);

    assert testMatrix.point.x >= -3 && testMatrix.point.y >= 0 && testMatrix.point.x < this.numColumns && testMatrix.point.y < this.numRows;
    if(testMatrix.numColumns >= this.numColumns || testMatrix.numRows >= this.numRows)
      return _COLLISION;
      
    if(isMatrixWithinBoundariesCollisioning(testMatrix)){
      return _COLLISION;
    }
    
    Logger.Log("No hubo colision");
    return _NO_COLLISION;
  }
  PointMatrix adjustMatrixAccordingToPosition(PointMatrix testMatrix){
    Point point = testMatrix.point;
    if(point.y >= 0)
      return testMatrix;
    MatrixManager newMatrix = MatrixManager.CharMatrix(testMatrix.Matrix);
    newMatrix.trimRows(abs(point.y));
    return PointMatrix.PointMatrix(newMatrix.Matrix, Point.Point(point.x,  0));
  }
  boolean isMatrixWithinBoundariesCollisioning(PointMatrix testMatrix){
    Point point = testMatrix.point;
    Logger.Log("Esta dentro del cuadro point.x:" + point.x + ", point.y:" + point.y + ". CM:" + this.Matrix.length + "x" + this.Matrix[0].length +
               "mx:" + testMatrix.numColumns + ", my:" + testMatrix.numRows);
    int my = 0;
    if(point.y <=0)
      my = abs(point.y) + 1;
    for(int i = 0; i < testMatrix.numColumns;i++){
      for(int j= my; j < testMatrix.numRows; j++){
        if(testMatrix.getValue(i,j) == _COLLISION_CHAR &&
          (this.Matrix[point.x + i][point.y + j] == _COLLISION_CHAR)){
          
          Logger.Log("Colision");
          return true;
        }
      }
    }
    return false;
  }
  
  void save(PointMatrix pMatrix){
    pMatrix = adjustMatrixAccordingToPosition(pMatrix);
    Point matrixRefPoint = pMatrix.point;
    if(this.hasBorder)
      matrixRefPoint.moveBoth(1);
    int matrixRefColumn = matrixRefPoint.x;
    int matrixRefRow = matrixRefPoint.y;

    assert matrixRefColumn > -4 && matrixRefRow > -1;
    assert pMatrix.Matrix.length > 0 && pMatrix.Matrix[0].length > 0;

    for(int col= matrixRefColumn; col < matrixRefColumn + pMatrix.numColumns; col++){
      for(int row= matrixRefRow; row < matrixRefRow + pMatrix.numRows; row++){
        if(pMatrix.getBool(col - matrixRefColumn, row - matrixRefRow)){
          Logger.Log("Guardada la posicion (" + col + ", " + row + ")");
          this.Matrix[col][row] = _COLLISION_CHAR;
        }
      }
    }
  }
  
  void Print(){
    if(Logger.active){
      MatrixManager charMatrix = MatrixManager.CharMatrixWithBorder(this.Matrix);
      charMatrix.Print();
    }
  }
  
  void manipulateGivenRows(String rows, int op){
    if(op == _DELETE){
      MatrixManager charMatrix = MatrixManager.CharMatrixWithBorder(this.Matrix);
      charMatrix.deleteGivenRows(rows);
      this.Matrix = charMatrix.Matrix;
    }
  }
}
